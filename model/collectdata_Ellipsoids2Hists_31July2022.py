## created 220723 from collectdata_kde_Ellipsoids_22Ajul2022.py
## which divides events into intervals for tracks-to-KDE learning

## will replace 
##   kernel = np.asarray(f["poca_KDE_A"]) + epsilon
## for definition of Y[] with
## --##  taken from collectdata_mds_A
## --            Y = np.asarray(f["pv"]).astype(dtype)
## --            if masking:
## --                # Set the result to nan if the "other" array is above
## --                # threshold and the current array is below threshold
## --                Y[(np.asarray(f["pv_other"]) > 0.01) & (Y < 0.01)] = dtype(np.nan)
##  from
##   collectdata_t2hists.py

##
##
## basic structure derived from collectdata_mdsA.py
## but adapted so that X will return a tensor built from
## the awkward arrays of track parameters (poca-ellipsoid values)
## to be used as the feature set for algorithm and Y will return the 
## PV target histogram values.
##
##  building X requires "padding" the awkward array content so it
##  fits into a well-defined tensor structure.
##
##  this version breaks the dataset into nIntervals of binsPerInterval
##  per event rather than a single 4000 bin dataset per event.


import torch
from torch.utils.data import TensorDataset

import numpy as np
from pathlib import Path
from functools import partial
import warnings
from collections import namedtuple

from .utilities import Timer
from .jagged import concatenate

import matplotlib.pyplot as plt

## this contains the method 
##  six_ellipsoid_parameters(majorAxis,minorAxis_1,minorAxis_2)
from model.ellipsoids import six_ellipsoid_parameters

# This can throw a warning about float - let's hide it for now.
with warnings.catch_warnings():
    warnings.simplefilter("ignore", category=FutureWarning)
    import h5py

try:
    import awkward0 as awkward
    ja = awkward.JaggedArray
except ModuleNotFoundError:
    import awkward
    ja = awkward.Array

## mds 220731 ja = awkward.JaggedArray

dtype_X = np.float32  ## set to float32 for use on CPU; can set to float16 for GPU
dtype_Y = np.float32  ## set to float32 for use on CPU; can set to float16 for GPU

VertexInfo = namedtuple("VertexInfo", ("x", "y", "z", "n", "cat"))

def collect_t2hists_data(
    *files,
    batch_size=1,
    dtype=np.float32,
    device=None,
    masking = False,
    slice=None,
    **kargs,
):
    """
    This function collects data. It does not split it up. You can pass in multiple files.
    Example: collect_data('a.h5', 'b.h5')

    batch_size: The number of events per batch
    dtype: Select a different dtype (like float16)
    slice: Allow just a slice of data to be loaded
    device: The device to load onto (CPU by default)
    **kargs: Any other keyword arguments will be passed on to torch's DataLoader
    """

## these unit vectors will be used to convert the elements of 
## the ellipsoid major and minor axis vectors into vectors
    xhat = np.array([1, 0, 0])
    yhat = np.array([0, 1, 0])
    zhat = np.array([0, 0, 1])

    Xlist = []
    Ylist = []

    Xlist_ints = []
    Ylist_ints = []

    print("Loading data...")

    for XY_file in files:
        msg = f"Loaded {XY_file} in {{time:.4}} s"
        with Timer(msg), h5py.File(XY_file, mode="r") as f:
            ## [:,np.newaxis,:] makes X (a x b) --> (a x 1 x b) (axis 0, axis 1, axis 2)
            ## a is *probably* 4000 and b is *probably* N, but it could be the other
            ## way around;  check iwth .shape


##  taken from collectdata_mds_A using targetHist rather than Y
##  as name of array being created
            targetHist = np.asarray(f["pv"]).astype(dtype)
            if masking:
                # Set the result to nan if the "other" array is above
                # threshold and the current array is below threshold
                targetHist[(np.asarray(f["pv_other"]) > 0.01) & (targetHist < 0.01)] = dtype(np.nan)



            print("  ")
            print("targetHist.shape =      ",targetHist.shape)
            nEvts = targetHist.shape[0]
            nBins = targetHist.shape[1]
            binsPerInterval = int(100)
            nIntervals = int(nBins/binsPerInterval)
            print("binsPerInterval = ",binsPerInterval)
            print("nIntervals =       ",nIntervals)
            if (nBins != (binsPerInterval*nIntervals)):
              print("nBins = ",nBins)
              print("binsPerInteral*nIntervals = ",binsPerInteral*nIntervals)

            intervalHists = np.reshape(targetHist,(nEvts*nIntervals,binsPerInterval))
##            Y_intervals     = ja.concatenate((intervalHists),axis=1).astype(dtype_Y)
            Y_intervals     = intervalHists.astype(dtype_Y)


            print("intervalHists.shape = ",intervalHists.shape)
            print("Y_intervals.shape   = ",Y_intervals.shape)


        
            afile = awkward.hdf5(f)

##  we probably want to maintain scales in mm everywhere
##  or consistently rescale all of x,y,z,A,B, etc.            
            pocaz = np.asarray(afile["poca_z"].astype(dtype_X))
            pocax = np.asarray(afile["poca_x"].astype(dtype_X))
            pocay = np.asarray(afile["poca_y"].astype(dtype_X))
            pocaMx = np.asarray(afile["major_axis_x"].astype(dtype_X))
            print("pocaMx.shape = ", pocaMx.shape)
            pocaMy = np.asarray(afile["major_axis_y"].astype(dtype_X))
            pocaMz = np.asarray(afile["major_axis_z"].astype(dtype_X))

## 220721  mds define nIntervalTracks as a standard Python array to be "filled" for each interval
##             at the end of the method, a histogram can be created
            nIntervalTracks = []
            nIntervalGoodTracks = []
            nIntervalVeryGoodTracks = []

            nVeryGoodTracks = []   ## where this includes cuts on sigmaZ, nSigmaX, and nSigmaY

## create numbers of bins and histogram ranges for numpy histograms to be built,
## then empty histograms

            h_SigmaZ_bins  = 100
            h_nSigmaX_bins = 100
            h_nSigmaY_bins = 100
          
            h_SigmaZ_range  = (0.,10.)
            h_nSigmaX_range = (-20.,20.) 
            h_nSigmaY_range = (-20.,20.)

            empty = []
            h_SigmaZ,  bin_edges_SigmaZ  = np.histogram(empty, h_SigmaZ_bins, h_SigmaZ_range)
            h_nSigmaX, bin_edges_nSigmaX = np.histogram(empty, h_nSigmaX_bins, h_nSigmaX_range)
            h_nSigmaY, bin_edges_nSigmaY = np.histogram(empty, h_nSigmaY_bins, h_nSigmaY_range)
##

            nEvts = len(pocaz)
            print("nEvts = ", nEvts)
            print("pocaz.shape = ",pocaz.shape)

            print("len(pocaMx[0]) = ", len(pocaMx[0]))
            print("len(pocaMx[1]) = ", len(pocaMx[1]))
            print("len(pocaMx[2]) = ", len(pocaMx[2]))
            print("len(pocaMx[3]) = ", len(pocaMx[3]))
            print("len(pocaMx[4]) = ", len(pocaMx[4]))

            Mx = np.multiply(pocaMx.reshape(nEvts,1),xhat)
            My = np.multiply(pocaMy.reshape(nEvts,1),yhat)
            Mz = np.multiply(pocaMz.reshape(nEvts,1),zhat)
            majorAxis = Mx+My+Mz
## mds            print("majorAxis.shape = ",majorAxis.shape)


            poca_m1x = np.asarray(afile["minor_axis1_x"].astype(dtype_Y))
            poca_m1y = np.asarray(afile["minor_axis1_y"].astype(dtype_Y))
            poca_m1z = np.asarray(afile["minor_axis1_z"].astype(dtype_Y))

            mx = np.multiply(poca_m1x.reshape(nEvts,1),xhat)
            my = np.multiply(poca_m1y.reshape(nEvts,1),yhat)
            mz = np.multiply(poca_m1z.reshape(nEvts,1),zhat)
            minorAxis_1 = mx+my+mz
## mds            print("minorAxis_1.shape = ",minorAxis_1.shape)

            poca_m2x = np.asarray(afile["minor_axis2_x"].astype(dtype_Y))
            poca_m2y = np.asarray(afile["minor_axis2_y"].astype(dtype_Y))
            poca_m2z = np.asarray(afile["minor_axis2_z"].astype(dtype_Y))


            mx = np.multiply(poca_m2x.reshape(nEvts,1),xhat)
            my = np.multiply(poca_m2y.reshape(nEvts,1),yhat)
            mz = np.multiply(poca_m2z.reshape(nEvts,1),zhat)
            minorAxis_2 = mx+my+mz
## mds            print("minorAxis_2.shape = ",minorAxis_1.shape)


            A, B, C, D, E, F = six_ellipsoid_parameters(majorAxis,minorAxis_1,minorAxis_2)

## mds            print("A.shape = ",A.shape)
            for iTrk in range(0):
              print("majorAxis[iTrk][0][0] = ",majorAxis[iTrk][0][0])
              print("majorAxis[iTrk][1][0] = ",majorAxis[iTrk][1][0])
              print("majorAxis[iTrk][2][0] = ",majorAxis[iTrk][2][0])
              print("minorAxis_1[iTrk][0][0] = ",minorAxis_1[iTrk][0][0])
              print("minorAxis_1[iTrk][1][0] = ",minorAxis_1[iTrk][1][0])
              print("minorAxis_1[iTrk][2][0] = ",minorAxis_1[iTrk][2][0])
              print("minorAxis_2[iTrk][0][0] = ",minorAxis_2[iTrk][0][0])
              print("minorAxis_2[iTrk][1][0] = ",minorAxis_2[iTrk][1][0])
              print("minorAxis_2[iTrk][2][0] = ",minorAxis_2[iTrk][2][0])
              print("  ")

## add some "debugging" code to make sure I understand enumerate
##  mds 220711

            minZ = -100.
            maxZ =  300.
            intervalLength = (maxZ-minZ)/nIntervals
            print(" *** intervalLength in mm = ",intervalLength,"   ***")

##  mark non-track data with -99 as a flag
            maxIntLen = 120  ## to be re-visited  mds 220712
            padded_int_pocaz   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocax   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocay   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaA   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaB   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaC   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaD   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaE   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaF   = np.zeros((nEvts*nIntervals,maxIntLen))-99.

            for  eventIndex, e in enumerate(pocaz):
              if (eventIndex<1):
                print("eventIndex = ",eventIndex)
              local_pocaz = pocaz[eventIndex][:]
              local_pocax = pocax[eventIndex][:]
              local_pocay = pocay[eventIndex][:]
              local_A = A[eventIndex][:]
              local_B = B[eventIndex][:]
              local_C = C[eventIndex][:]
              local_D = D[eventIndex][:]
              local_E = E[eventIndex][:]
              local_F = F[eventIndex][:]

              if (eventIndex<0):
                print("local_A.shape = ",local_A.shape)
                print("local_A =       ",local_A)
  
              indices = np.argsort(local_pocaz)

              ordered_pocaz = local_pocaz[indices]
              ordered_pocax = local_pocax[indices]
              ordered_pocay = local_pocay[indices]
              ordered_A     = local_A[indices]
              ordered_B     = local_B[indices]
              ordered_C     = local_C[indices]
              ordered_D     = local_D[indices]
              ordered_E     = local_E[indices]
              ordered_F     = local_F[indices]
  
              if (eventIndex<0): 
                print("len(local_pocaz) = ",len(local_pocaz))
                print("  ")
                print("local_pocaz = ",local_pocaz)
                print("ordered_pocaz = ",ordered_pocaz) 
                print("      -----------      ")
                print("local_pocax = ",local_pocax)
                print("ordered_pocax = ",ordered_pocax)
                print("  ---------------------- \n")

              for interval in range(nIntervals):
                interval_lowEdge  = minZ + interval*intervalLength
                interval_highEdge = interval_lowEdge + intervalLength 
                interval_minZ     = interval_lowEdge - 2.5
                interval_maxZ     = interval_highEdge + 2.5
                if (eventIndex<0):
                    print(" -- interval, interval_minZ, interval_maxZ = ",interval, interval_minZ, interval_maxZ)
                intervalRange = (local_pocaz>interval_minZ) & (local_pocaz<interval_maxZ)
## for each interval we want the values of z shifted to be centered at the
## center of the interval
                interval_pocaz = local_pocaz[intervalRange] - interval_lowEdge
                interval_pocax = local_pocax[intervalRange]
                interval_pocay = local_pocay[intervalRange]
                interval_A     = local_A[intervalRange]
                interval_B     = local_B[intervalRange]
                interval_C     = local_C[intervalRange]
                interval_D     = local_D[intervalRange]
                interval_E     = local_E[intervalRange]
                interval_F     = local_F[intervalRange]
               
                intervalSigmaZ = np.sqrt(np.divide(1.,interval_C))
                intervalSigmaX = np.sqrt(np.divide(1.,interval_A))
                intervalSigmaY = np.sqrt(np.divide(1.,interval_B))
                xSigmas = np.divide(interval_pocax,intervalSigmaX)
                ySigmas = np.divide(interval_pocay,intervalSigmaY)

                veryGoodTracks =  (intervalSigmaZ<2.0) & (np.absolute(xSigmas)<4.0) & (np.absolute(ySigmas)<4.0)

##  let's try restricting the following calcualtions to the
##  first 1000 events to reduce the computational load
                if (eventIndex<1000):
## mds 220721  keep track of the number of tracks in this interval
                    nIntervalTracks.append(len(interval_pocaz))
    
    
                    interval_SigmaZ,  bin_edges  = np.histogram(intervalSigmaZ, h_SigmaZ_bins, h_SigmaZ_range)
                    h_SigmaZ = h_SigmaZ + interval_SigmaZ
    
                    interval_nSigmaX,  bin_edges  = np.histogram(xSigmas, h_nSigmaX_bins, h_nSigmaX_range)
                    h_nSigmaX = h_nSigmaX + interval_nSigmaX
    
                    interval_nSigmaY,  bin_edges  = np.histogram(ySigmas, h_nSigmaY_bins, h_nSigmaY_range)
                    h_nSigmaY = h_nSigmaY + interval_nSigmaY
    
                    interval_good_pocaz = interval_pocaz[intervalSigmaZ<2.0]
                    interval_very_good_pocaz = interval_pocaz[intervalSigmaZ<1.0]
    
                    nIntervalGoodTracks.append(len(interval_good_pocaz))
                    nIntervalVeryGoodTracks.append(len(interval_very_good_pocaz))
    
                if (eventIndex<0): 
                    print("  ")
                    if (interval<0):
                      print("eventIndex, interval = ",eventIndex, interval)
                      print("interval_pocaz = ",interval_pocaz)
                      print("             ----          ")
                      print("interval_pocax = ",interval_pocax)

## and now for all intervals for the eventIndex range
                    print("  ")
                    print("eventIndex and interval = ",eventIndex,interval) 
                    print("interval_pocaz = ",interval_pocaz)

                interval_pocaz = interval_pocaz[veryGoodTracks]
                interval_pocax = interval_pocax[veryGoodTracks]
                interval_pocay = interval_pocay[veryGoodTracks]
                interval_A     = interval_A[veryGoodTracks]
                interval_B     = interval_B[veryGoodTracks]
                interval_C     = interval_C[veryGoodTracks]
                interval_D     = interval_D[veryGoodTracks]
                interval_E     = interval_E[veryGoodTracks]
                interval_F     = interval_F[veryGoodTracks]

                nVeryGoodTracks.append(len(interval_pocaz))

                fillingLength = min(len(interval_pocaz),maxIntLen)
                ii = eventIndex*nIntervals + interval
                padded_int_pocaz[ii,:fillingLength] = interval_pocaz[:fillingLength].astype(dtype_Y)
                padded_int_pocax[ii,:fillingLength] = interval_pocax[:fillingLength].astype(dtype_Y)
                padded_int_pocay[ii,:fillingLength] = interval_pocay[:fillingLength].astype(dtype_Y)
                padded_int_pocaA[ii,:fillingLength] = interval_A[:fillingLength].astype(dtype_Y)
                padded_int_pocaB[ii,:fillingLength] = interval_B[:fillingLength].astype(dtype_Y)
                padded_int_pocaC[ii,:fillingLength] = interval_C[:fillingLength].astype(dtype_Y)
                padded_int_pocaD[ii,:fillingLength] = interval_D[:fillingLength].astype(dtype_Y)
                padded_int_pocaE[ii,:fillingLength] = interval_E[:fillingLength].astype(dtype_Y)
                padded_int_pocaF[ii,:fillingLength] = interval_F[:fillingLength].astype(dtype_Y)

################                


            padded_int_pocaz  = padded_int_pocaz[:,np.newaxis,:]
            padded_int_pocax  = padded_int_pocax[:,np.newaxis,:]
            padded_int_pocay  = padded_int_pocay[:,np.newaxis,:]
            padded_int_pocaA  = padded_int_pocaA[:,np.newaxis,:]
            padded_int_pocaB  = padded_int_pocaB[:,np.newaxis,:]
            padded_int_pocaC  = padded_int_pocaC[:,np.newaxis,:]
            padded_int_pocaD  = padded_int_pocaD[:,np.newaxis,:]
            padded_int_pocaE  = padded_int_pocaE[:,np.newaxis,:]
            padded_int_pocaF  = padded_int_pocaF[:,np.newaxis,:]


            X_ints = ja.concatenate((padded_int_pocaz,padded_int_pocax,padded_int_pocay,padded_int_pocaA,padded_int_pocaB,padded_int_pocaC,padded_int_pocaD,padded_int_pocaE,padded_int_pocaF),axis=1).astype(dtype_X)

            print("len(X_ints) =",len(X_ints))

            Xlist_ints.append(X_ints)
            Ylist_ints.append(Y_intervals)

            print("len(Xlist_ints) = ",len(Xlist_ints))

    X_intervals = np.concatenate(Xlist_ints, axis = 0)
    Y_intervals = np.concatenate(Ylist_ints, axis = 0)

    if slice:

        X_intervals = X_intervals[slice, :]
        Y_intervals = Y_intervals[slice, :]

    with Timer(start=f"Constructing {X_intervals.shape[0]} event dataset"):

        x_t_intervals = torch.tensor(X_intervals)
        y_t_intervals = torch.tensor(Y_intervals)

##  for debugging
        for intervalIndex in range(00):
          print("  ")
          print(" ** intervalIndex = ",intervalIndex)
          print("y_t_intervals[intervalIndex][0:100] = ")
          print(y_t_intervals[intervalIndex][0:100])
          print("  ")
          print("x_t_intervals[intervalIndex][0][0:20] = ")
          print(x_t_intervals[intervalIndex][0][0:20])
          print(" --------- ")


        if device is not None:

            x_t_intervals = x_t_intervals.to(device)
            y_t_intervals = y_t_intervals.to(device)

        dataset = TensorDataset(x_t_intervals, y_t_intervals)

    loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, **kargs)

    print("x_t_intervals.shape = ",x_t_intervals.shape)
    print("x_t_intervals.shape[0] = ", x_t_intervals.shape[0])
    print("x_t_intervals.shape[1] = ", x_t_intervals.shape[1])


    print("y_t_intervals.shape = ",y_t_intervals.shape)
    print("y_t_intervals.shape[0] = ", y_t_intervals.shape[0])
    print("y_t_intervals.shape[1] = ", y_t_intervals.shape[1])

    fig_size = plt.rcParams["figure.figsize"]
    fig_size[0] = 8
    fig_size[1] = 5
    plt.rcParams["figure.figsize"] = fig_size   
    
    plt.figure()
    nC, binsC, patches = plt.hist(nIntervalTracks, bins=100, range=[0,300], density=False, facecolor="b", alpha=0.75)
    plt.xlabel('nTracks/interval')
    plt.ylabel('events per  3')
    plt.title('interval nTracks')
    plt.axis([0.,300.,0.1,1.5*max(nC)])
    plt.grid(True)
    plt.yscale('log')
    plt.savefig("nIntervalTracks.png")
    plt.close()

##
 
    plt.figure()
    nC, binsC, patches = plt.hist(nIntervalGoodTracks, bins=100, range=[0,300], density=False, facecolor="green", alpha=0.75)
    plt.xlabel('nTracks/interval')
    plt.ylabel('events per  3')
    plt.title('interval nTracks with sigmaZ<2.0 mm')
    plt.axis([0.,300.,0.1,1.5*max(nC)])
    plt.grid(True)
    plt.yscale('log')
    plt.savefig("nIntervalGoodTracks.png")
    plt.close()
 
##
 
    plt.figure()
    nC, binsC, patches = plt.hist(nIntervalVeryGoodTracks, bins=100, range=[0,300], density=False, facecolor="green", alpha=0.75)
    plt.xlabel('nTracks/interval')
    plt.ylabel('events per  3')
    plt.title('interval nTracks with sigmaZ<1.0 mm')
    plt.axis([0.,300.,0.1,1.5*max(nC)])
    plt.grid(True)
    plt.yscale('log')
    plt.savefig("nIntervalVeryGoodTracks.png")
    plt.close()

##
 
    plt.figure()
    nC, binsC, patches = plt.hist(nVeryGoodTracks, bins=100, range=[0,300], density=False, facecolor="cyan", alpha=0.75)
    plt.xlabel('nTracks/interval')
    plt.ylabel('events per  3')
    plt.title('interval nTracks with sigmaZ<1.0 mm')
    plt.axis([0.,300.,0.1,1.5*max(nC)])
    plt.grid(True)
    plt.yscale('log')
    plt.savefig("nVeryGoodTracks.png")
    plt.close()
 
##

    plt.bar(bin_edges_SigmaZ[:-1],h_SigmaZ,width=np.diff(bin_edges_SigmaZ),edgecolor="blue",align="edge")
    plt.xlabel(' sigmaZ [mm] ')
    plt.ylabel(' number per 100 microns ')
    plt.title('  sigmaZ distribution ')
    plt.axis([0.,10.,0.1,1.5*max(h_SigmaZ)])
    plt.grid(True)
    plt.yscale('log')
    plt.savefig("sigmaZ.png")
    plt.close()

##

    plt.bar(bin_edges_nSigmaX[:-1],h_nSigmaX,width=np.diff(bin_edges_nSigmaX),edgecolor="blue",align="edge")
    plt.xlabel(' x displacement in units of sigmaX ')
    plt.ylabel(' dN/dsigma per 0.4 ')
    plt.title('  x displacement distribution ')
    plt.axis([-20.,20.,0.,1.15*max(h_nSigmaX)])
    plt.grid(True)
    plt.yscale('linear')
    plt.savefig("nSigmaX.png")
    plt.close()

##

    plt.bar(bin_edges_nSigmaY[:-1],h_nSigmaY,width=np.diff(bin_edges_nSigmaY),edgecolor="blue",align="edge")
    plt.xlabel(' y displacement in units of sigmaY ')
    plt.ylabel(' dN/dsigma per 0.4 ')
    plt.title('  y displacement distribution ')
    plt.axis([-20.,20.,0.,1.15*max(h_nSigmaY)])
    plt.grid(True)
    plt.yscale('linear')
    plt.savefig("nSigmaY.png")
    plt.close()


    return loader

####### -----------------

def collect_t2hists_data_128bins(
    *files,
    batch_size=1,
    dtype=np.float32,
    device=None,
    masking = False,
    slice=None,
    **kargs,
):
    """
    This function collects data. It does not split it up. You can pass in multiple files.
    Example: collect_data('a.h5', 'b.h5')

    batch_size: The number of events per batch
    dtype: Select a different dtype (like float16)
    slice: Allow just a slice of data to be loaded
    device: The device to load onto (CPU by default)
    **kargs: Any other keyword arguments will be passed on to torch's DataLoader
    """

## these unit vectors will be used to convert the elements of 
## the ellipsoid major and minor axis vectors into vectors
    xhat = np.array([1, 0, 0])
    yhat = np.array([0, 1, 0])
    zhat = np.array([0, 0, 1])

    Xlist = []
    Ylist = []

    Xlist_ints = []
    Ylist_ints = []

    print("Loading data...")

    for XY_file in files:
        msg = f"Loaded {XY_file} in {{time:.4}} s"
        with Timer(msg), h5py.File(XY_file, mode="r") as f:
            ## [:,np.newaxis,:] makes X (a x b) --> (a x 1 x b) (axis 0, axis 1, axis 2)
            ## a is *probably* 4000 and b is *probably* N, but it could be the other
            ## way around;  check iwth .shape


##  taken from collectdata_mds_A using targetHist rather than Y
##  as name of array being created
            targetHist = np.asarray(f["pv"]).astype(dtype)
            if masking:
                # Set the result to nan if the "other" array is above
                # threshold and the current array is below threshold
                targetHist[(np.asarray(f["pv_other"]) > 0.01) & (targetHist < 0.01)] = dtype(np.nan)



            print("  ")
            print("targetHist.shape =      ",targetHist.shape)
            nEvts = targetHist.shape[0]
            nBins = targetHist.shape[1]
            binsPerInterval = int(128)
            nIntervals = int(nBins/binsPerInterval)
            print("binsPerInterval = ",binsPerInterval)
            print("nIntervals =       ",nIntervals)
            if (nBins != (binsPerInterval*nIntervals)):
              print("nBins = ",nBins)
              print("binsPerInteral*nIntervals = ",binsPerInterval*nIntervals)

            newBins = binsPerInterval*nIntervals
            targetHist = targetHist[:,:newBins]
            intervalHists = np.reshape(targetHist,(nEvts*nIntervals,binsPerInterval))
##            Y_intervals     = ja.concatenate((intervalHists),axis=1).astype(dtype_Y)
            Y_intervals     = intervalHists.astype(dtype_Y)


            print("intervalHists.shape = ",intervalHists.shape)
            print("Y_intervals.shape   = ",Y_intervals.shape)


        
            afile = awkward.hdf5(f)

##  we probably want to maintain scales in mm everywhere
##  or consistently rescale all of x,y,z,A,B, etc.            
            pocaz = np.asarray(afile["poca_z"].astype(dtype_X))
            pocax = np.asarray(afile["poca_x"].astype(dtype_X))
            pocay = np.asarray(afile["poca_y"].astype(dtype_X))
            pocaMx = np.asarray(afile["major_axis_x"].astype(dtype_X))
            print("pocaMx.shape = ", pocaMx.shape)
            pocaMy = np.asarray(afile["major_axis_y"].astype(dtype_X))
            pocaMz = np.asarray(afile["major_axis_z"].astype(dtype_X))

## 220721  mds define nIntervalTracks as a standard Python array to be "filled" for each interval
##             at the end of the method, a histogram can be created
            nIntervalTracks = []
            nIntervalGoodTracks = []
            nIntervalVeryGoodTracks = []

            nVeryGoodTracks = []   ## where this includes cuts on sigmaZ, nSigmaX, and nSigmaY

## create numbers of bins and histogram ranges for numpy histograms to be built,
## then empty histograms

            h_SigmaZ_bins  = 100
            h_nSigmaX_bins = 100
            h_nSigmaY_bins = 100
          
            h_SigmaZ_range  = (0.,10.)
            h_nSigmaX_range = (-20.,20.) 
            h_nSigmaY_range = (-20.,20.)

            empty = []
            h_SigmaZ,  bin_edges_SigmaZ  = np.histogram(empty, h_SigmaZ_bins, h_SigmaZ_range)
            h_nSigmaX, bin_edges_nSigmaX = np.histogram(empty, h_nSigmaX_bins, h_nSigmaX_range)
            h_nSigmaY, bin_edges_nSigmaY = np.histogram(empty, h_nSigmaY_bins, h_nSigmaY_range)
##

            nEvts = len(pocaz)
            print("nEvts = ", nEvts)
            print("pocaz.shape = ",pocaz.shape)

            print("len(pocaMx[0]) = ", len(pocaMx[0]))
            print("len(pocaMx[1]) = ", len(pocaMx[1]))
            print("len(pocaMx[2]) = ", len(pocaMx[2]))
            print("len(pocaMx[3]) = ", len(pocaMx[3]))
            print("len(pocaMx[4]) = ", len(pocaMx[4]))

            Mx = np.multiply(pocaMx.reshape(nEvts,1),xhat)
            My = np.multiply(pocaMy.reshape(nEvts,1),yhat)
            Mz = np.multiply(pocaMz.reshape(nEvts,1),zhat)
            majorAxis = Mx+My+Mz
## mds            print("majorAxis.shape = ",majorAxis.shape)


            poca_m1x = np.asarray(afile["minor_axis1_x"].astype(dtype_Y))
            poca_m1y = np.asarray(afile["minor_axis1_y"].astype(dtype_Y))
            poca_m1z = np.asarray(afile["minor_axis1_z"].astype(dtype_Y))

            mx = np.multiply(poca_m1x.reshape(nEvts,1),xhat)
            my = np.multiply(poca_m1y.reshape(nEvts,1),yhat)
            mz = np.multiply(poca_m1z.reshape(nEvts,1),zhat)
            minorAxis_1 = mx+my+mz
## mds            print("minorAxis_1.shape = ",minorAxis_1.shape)

            poca_m2x = np.asarray(afile["minor_axis2_x"].astype(dtype_Y))
            poca_m2y = np.asarray(afile["minor_axis2_y"].astype(dtype_Y))
            poca_m2z = np.asarray(afile["minor_axis2_z"].astype(dtype_Y))


            mx = np.multiply(poca_m2x.reshape(nEvts,1),xhat)
            my = np.multiply(poca_m2y.reshape(nEvts,1),yhat)
            mz = np.multiply(poca_m2z.reshape(nEvts,1),zhat)
            minorAxis_2 = mx+my+mz
## mds            print("minorAxis_2.shape = ",minorAxis_1.shape)


            A, B, C, D, E, F = six_ellipsoid_parameters(majorAxis,minorAxis_1,minorAxis_2)

## mds            print("A.shape = ",A.shape)
            for iTrk in range(0):
              print("majorAxis[iTrk][0][0] = ",majorAxis[iTrk][0][0])
              print("majorAxis[iTrk][1][0] = ",majorAxis[iTrk][1][0])
              print("majorAxis[iTrk][2][0] = ",majorAxis[iTrk][2][0])
              print("minorAxis_1[iTrk][0][0] = ",minorAxis_1[iTrk][0][0])
              print("minorAxis_1[iTrk][1][0] = ",minorAxis_1[iTrk][1][0])
              print("minorAxis_1[iTrk][2][0] = ",minorAxis_1[iTrk][2][0])
              print("minorAxis_2[iTrk][0][0] = ",minorAxis_2[iTrk][0][0])
              print("minorAxis_2[iTrk][1][0] = ",minorAxis_2[iTrk][1][0])
              print("minorAxis_2[iTrk][2][0] = ",minorAxis_2[iTrk][2][0])
              print("  ")

## add some "debugging" code to make sure I understand enumerate
##  mds 220711

            minZ = -100.
            maxZ =  300.
            intervalLength = (maxZ-minZ)/nIntervals
            print(" *** intervalLength in mm = ",intervalLength,"   ***")

##  mark non-track data with -99 as a flag
            maxIntLen = 150  ## to be re-visited  mds 220712 --> reset from 120 to 150 for 128 intervals
            padded_int_pocaz   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocax   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocay   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaA   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaB   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaC   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaD   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaE   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaF   = np.zeros((nEvts*nIntervals,maxIntLen))-99.

            for  eventIndex, e in enumerate(pocaz):
              if (eventIndex<1):
                print("eventIndex = ",eventIndex)
              local_pocaz = pocaz[eventIndex][:]
              local_pocax = pocax[eventIndex][:]
              local_pocay = pocay[eventIndex][:]
              local_A = A[eventIndex][:]
              local_B = B[eventIndex][:]
              local_C = C[eventIndex][:]
              local_D = D[eventIndex][:]
              local_E = E[eventIndex][:]
              local_F = F[eventIndex][:]

              if (eventIndex<0):
                print("local_A.shape = ",local_A.shape)
                print("local_A =       ",local_A)
  
              indices = np.argsort(local_pocaz)

              ordered_pocaz = local_pocaz[indices]
              ordered_pocax = local_pocax[indices]
              ordered_pocay = local_pocay[indices]
              ordered_A     = local_A[indices]
              ordered_B     = local_B[indices]
              ordered_C     = local_C[indices]
              ordered_D     = local_D[indices]
              ordered_E     = local_E[indices]
              ordered_F     = local_F[indices]
  
              if (eventIndex<0): 
                print("len(local_pocaz) = ",len(local_pocaz))
                print("  ")
                print("local_pocaz = ",local_pocaz)
                print("ordered_pocaz = ",ordered_pocaz) 
                print("      -----------      ")
                print("local_pocax = ",local_pocax)
                print("ordered_pocax = ",ordered_pocax)
                print("  ---------------------- \n")

              for interval in range(nIntervals):
                interval_lowEdge  = minZ + interval*intervalLength
                interval_highEdge = interval_lowEdge + intervalLength 
                interval_minZ     = interval_lowEdge - 2.5
                interval_maxZ     = interval_highEdge + 2.5
                if (eventIndex<0):
                    print(" -- interval, interval_minZ, interval_maxZ = ",interval, interval_minZ, interval_maxZ)
                intervalRange = (local_pocaz>interval_minZ) & (local_pocaz<interval_maxZ)
## for each interval we want the values of z shifted to be centered at the
## center of the interval
                interval_pocaz = local_pocaz[intervalRange] - interval_lowEdge
                interval_pocax = local_pocax[intervalRange]
                interval_pocay = local_pocay[intervalRange]
                interval_A     = local_A[intervalRange]
                interval_B     = local_B[intervalRange]
                interval_C     = local_C[intervalRange]
                interval_D     = local_D[intervalRange]
                interval_E     = local_E[intervalRange]
                interval_F     = local_F[intervalRange]
               
                intervalSigmaZ = np.sqrt(np.divide(1.,interval_C))
                intervalSigmaX = np.sqrt(np.divide(1.,interval_A))
                intervalSigmaY = np.sqrt(np.divide(1.,interval_B))
                xSigmas = np.divide(interval_pocax,intervalSigmaX)
                ySigmas = np.divide(interval_pocay,intervalSigmaY)

                veryGoodTracks =  (intervalSigmaZ<2.0) & (np.absolute(xSigmas)<4.0) & (np.absolute(ySigmas)<4.0)

##  let's try restricting the following calcualtions to the
##  first 1000 events to reduce the computational load
                if (eventIndex<1000):
## mds 220721  keep track of the number of tracks in this interval
                    nIntervalTracks.append(len(interval_pocaz))
    
    
                    interval_SigmaZ,  bin_edges  = np.histogram(intervalSigmaZ, h_SigmaZ_bins, h_SigmaZ_range)
                    h_SigmaZ = h_SigmaZ + interval_SigmaZ
    
                    interval_nSigmaX,  bin_edges  = np.histogram(xSigmas, h_nSigmaX_bins, h_nSigmaX_range)
                    h_nSigmaX = h_nSigmaX + interval_nSigmaX
    
                    interval_nSigmaY,  bin_edges  = np.histogram(ySigmas, h_nSigmaY_bins, h_nSigmaY_range)
                    h_nSigmaY = h_nSigmaY + interval_nSigmaY
    
                    interval_good_pocaz = interval_pocaz[intervalSigmaZ<2.0]
                    interval_very_good_pocaz = interval_pocaz[intervalSigmaZ<1.0]
    
                    nIntervalGoodTracks.append(len(interval_good_pocaz))
                    nIntervalVeryGoodTracks.append(len(interval_very_good_pocaz))
    
                if (eventIndex<0): 
                    print("  ")
                    if (interval<0):
                      print("eventIndex, interval = ",eventIndex, interval)
                      print("interval_pocaz = ",interval_pocaz)
                      print("             ----          ")
                      print("interval_pocax = ",interval_pocax)

## and now for all intervals for the eventIndex range
                    print("  ")
                    print("eventIndex and interval = ",eventIndex,interval) 
                    print("interval_pocaz = ",interval_pocaz)

                interval_pocaz = interval_pocaz[veryGoodTracks]
                interval_pocax = interval_pocax[veryGoodTracks]
                interval_pocay = interval_pocay[veryGoodTracks]
                interval_A     = interval_A[veryGoodTracks]
                interval_B     = interval_B[veryGoodTracks]
                interval_C     = interval_C[veryGoodTracks]
                interval_D     = interval_D[veryGoodTracks]
                interval_E     = interval_E[veryGoodTracks]
                interval_F     = interval_F[veryGoodTracks]

                nVeryGoodTracks.append(len(interval_pocaz))

                fillingLength = min(len(interval_pocaz),maxIntLen)
                ii = eventIndex*nIntervals + interval
                padded_int_pocaz[ii,:fillingLength] = interval_pocaz[:fillingLength].astype(dtype_Y)
                padded_int_pocax[ii,:fillingLength] = interval_pocax[:fillingLength].astype(dtype_Y)
                padded_int_pocay[ii,:fillingLength] = interval_pocay[:fillingLength].astype(dtype_Y)
                padded_int_pocaA[ii,:fillingLength] = interval_A[:fillingLength].astype(dtype_Y)
                padded_int_pocaB[ii,:fillingLength] = interval_B[:fillingLength].astype(dtype_Y)
                padded_int_pocaC[ii,:fillingLength] = interval_C[:fillingLength].astype(dtype_Y)
                padded_int_pocaD[ii,:fillingLength] = interval_D[:fillingLength].astype(dtype_Y)
                padded_int_pocaE[ii,:fillingLength] = interval_E[:fillingLength].astype(dtype_Y)
                padded_int_pocaF[ii,:fillingLength] = interval_F[:fillingLength].astype(dtype_Y)

################                


            padded_int_pocaz  = padded_int_pocaz[:,np.newaxis,:]
            padded_int_pocax  = padded_int_pocax[:,np.newaxis,:]
            padded_int_pocay  = padded_int_pocay[:,np.newaxis,:]
            padded_int_pocaA  = padded_int_pocaA[:,np.newaxis,:]
            padded_int_pocaB  = padded_int_pocaB[:,np.newaxis,:]
            padded_int_pocaC  = padded_int_pocaC[:,np.newaxis,:]
            padded_int_pocaD  = padded_int_pocaD[:,np.newaxis,:]
            padded_int_pocaE  = padded_int_pocaE[:,np.newaxis,:]
            padded_int_pocaF  = padded_int_pocaF[:,np.newaxis,:]


            X_ints = ja.concatenate((padded_int_pocaz,padded_int_pocax,padded_int_pocay,padded_int_pocaA,padded_int_pocaB,padded_int_pocaC,padded_int_pocaD,padded_int_pocaE,padded_int_pocaF),axis=1).astype(dtype_X)

            print("len(X_ints) =",len(X_ints))

            Xlist_ints.append(X_ints)
            Ylist_ints.append(Y_intervals)

            print("len(Xlist_ints) = ",len(Xlist_ints))

    X_intervals = np.concatenate(Xlist_ints, axis = 0)
    Y_intervals = np.concatenate(Ylist_ints, axis = 0)

    if slice:

        X_intervals = X_intervals[slice, :]
        Y_intervals = Y_intervals[slice, :]

    with Timer(start=f"Constructing {X_intervals.shape[0]} event dataset"):

        x_t_intervals = torch.tensor(X_intervals)
        y_t_intervals = torch.tensor(Y_intervals)

##  for debugging
        for intervalIndex in range(00):
          print("  ")
          print(" ** intervalIndex = ",intervalIndex)
          print("y_t_intervals[intervalIndex][0:100] = ")
          print(y_t_intervals[intervalIndex][0:100])
          print("  ")
          print("x_t_intervals[intervalIndex][0][0:20] = ")
          print(x_t_intervals[intervalIndex][0][0:20])
          print(" --------- ")


        if device is not None:

            x_t_intervals = x_t_intervals.to(device)
            y_t_intervals = y_t_intervals.to(device)

        dataset = TensorDataset(x_t_intervals, y_t_intervals)

    loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, **kargs)

    print("x_t_intervals.shape = ",x_t_intervals.shape)
    print("x_t_intervals.shape[0] = ", x_t_intervals.shape[0])
    print("x_t_intervals.shape[1] = ", x_t_intervals.shape[1])


    print("y_t_intervals.shape = ",y_t_intervals.shape)
    print("y_t_intervals.shape[0] = ", y_t_intervals.shape[0])
    print("y_t_intervals.shape[1] = ", y_t_intervals.shape[1])

    fig_size = plt.rcParams["figure.figsize"]
    fig_size[0] = 8
    fig_size[1] = 5
    plt.rcParams["figure.figsize"] = fig_size   
    
    plt.figure()
    nC, binsC, patches = plt.hist(nIntervalTracks, bins=100, range=[0,300], density=False, facecolor="b", alpha=0.75)
    plt.xlabel('nTracks/interval')
    plt.ylabel('events per  3')
    plt.title('interval nTracks')
    plt.axis([0.,300.,0.1,1.5*max(nC)])
    plt.grid(True)
    plt.yscale('log')
    plt.savefig("nIntervalTracks.png")
    plt.close()

##
 
    plt.figure()
    nC, binsC, patches = plt.hist(nIntervalGoodTracks, bins=100, range=[0,300], density=False, facecolor="green", alpha=0.75)
    plt.xlabel('nTracks/interval')
    plt.ylabel('events per  3')
    plt.title('interval nTracks with sigmaZ<2.0 mm')
    plt.axis([0.,300.,0.1,1.5*max(nC)])
    plt.grid(True)
    plt.yscale('log')
    plt.savefig("nIntervalGoodTracks.png")
    plt.close()
 
##
 
    plt.figure()
    nC, binsC, patches = plt.hist(nIntervalVeryGoodTracks, bins=100, range=[0,300], density=False, facecolor="green", alpha=0.75)
    plt.xlabel('nTracks/interval')
    plt.ylabel('events per  3')
    plt.title('interval nTracks with sigmaZ<1.0 mm')
    plt.axis([0.,300.,0.1,1.5*max(nC)])
    plt.grid(True)
    plt.yscale('log')
    plt.savefig("nIntervalVeryGoodTracks.png")
    plt.close()

##
 
    plt.figure()
    nC, binsC, patches = plt.hist(nVeryGoodTracks, bins=100, range=[0,300], density=False, facecolor="cyan", alpha=0.75)
    plt.xlabel('nTracks/interval')
    plt.ylabel('events per  3')
    plt.title('interval nTracks with sigmaZ<1.0 mm')
    plt.axis([0.,300.,0.1,1.5*max(nC)])
    plt.grid(True)
    plt.yscale('log')
    plt.savefig("nVeryGoodTracks.png")
    plt.close()
 
##

    plt.bar(bin_edges_SigmaZ[:-1],h_SigmaZ,width=np.diff(bin_edges_SigmaZ),edgecolor="blue",align="edge")
    plt.xlabel(' sigmaZ [mm] ')
    plt.ylabel(' number per 100 microns ')
    plt.title('  sigmaZ distribution ')
    plt.axis([0.,10.,0.1,1.5*max(h_SigmaZ)])
    plt.grid(True)
    plt.yscale('log')
    plt.savefig("sigmaZ.png")
    plt.close()

##

    plt.bar(bin_edges_nSigmaX[:-1],h_nSigmaX,width=np.diff(bin_edges_nSigmaX),edgecolor="blue",align="edge")
    plt.xlabel(' x displacement in units of sigmaX ')
    plt.ylabel(' dN/dsigma per 0.4 ')
    plt.title('  x displacement distribution ')
    plt.axis([-20.,20.,0.,1.15*max(h_nSigmaX)])
    plt.grid(True)
    plt.yscale('linear')
    plt.savefig("nSigmaX.png")
    plt.close()

##

    plt.bar(bin_edges_nSigmaY[:-1],h_nSigmaY,width=np.diff(bin_edges_nSigmaY),edgecolor="blue",align="edge")
    plt.xlabel(' y displacement in units of sigmaY ')
    plt.ylabel(' dN/dsigma per 0.4 ')
    plt.title('  y displacement distribution ')
    plt.axis([-20.,20.,0.,1.15*max(h_nSigmaY)])
    plt.grid(True)
    plt.yscale('linear')
    plt.savefig("nSigmaY.png")
    plt.close()


    return loader

####### -----------------


def collect_truth(*files, pvs=True):
    """
    This function collects the truth information from files as
    awkward arrays (JaggedArrays). Give it the same files as collect_data.

    pvs: Collect PVs or SVs (default True: PVs)
    """

    x_list = []
    y_list = []
    z_list = []
    n_list = []
    c_list = []

    p = "p" if pvs else "s"

    for XY_file in files:
        msg = f"Loaded {XY_file} in {{time:.4}} s"
        with Timer(msg), h5py.File(XY_file, mode="r") as XY:
            afile = awkward.hdf5(XY)
            x_list.append(afile[f"{p}v_loc_x"])
            y_list.append(afile[f"{p}v_loc_y"])
            z_list.append(afile[f"{p}v_loc"])
            n_list.append(afile[f"{p}v_ntracks"])
            c_list.append(afile[f"{p}v_cat"])

    return VertexInfo(
        concatenate(x_list),
        concatenate(y_list),
        concatenate(z_list),
        concatenate(n_list),
        concatenate(c_list),
    )

