## derived from collectdata_kde_Ellipsoids_MinBiasMagUp_14Nov.py
## It writes out the numpy arrays X and Y that will be the input features 
## and labels  from which DataSets are created.


## collectdata_kde_Ellipsoids_aug2022_debug.py derived from collectdata_kde_Ellipsoids_aug2022.py

## use ordered pocaz, etc., to fill the padded arrays --> torch tensors
## require veryGoodTracks for tracks going to feature sets




## mds July 19,2020
##
## basic structure derived from collectdata_mdsA.py
## but adapted so that X will return a tensor built from
## the awkward arrays of track parameters
## to be used as the feature set for algorithm and Y will return the 
## KDE and associate Xmax and Ymax values to be used in the cost function.
##
##  building X requires "padding" the awkward array content so it
##  fits into a well-defined tensor structure.
##

import torch
from torch.utils.data import TensorDataset

import numpy as np
from pathlib import Path
from functools import partial
import warnings
from collections import namedtuple

from .utilities import Timer
from .jagged import concatenate

## add 220817 for scrubbing code
import math


## this contains the method 
##  six_ellipsoid_parameters(majorAxis,minorAxis_1,minorAxis_2)
from model.ellipsoids import six_ellipsoid_parameters

# This can throw a warning about float - let's hide it for now.
with warnings.catch_warnings():
    warnings.simplefilter("ignore", category=FutureWarning)
    import h5py

try:
    import awkward0 as awkward
except ModuleNotFoundError:
    import awkward

ja = awkward.JaggedArray

dtype_X = np.float32  ## set to float32 for use on CPU; can set to float16 for GPU
dtype_Y = np.float32  ## set to float32 for use on CPU; can set to float16 for GPU

VertexInfo = namedtuple("VertexInfo", ("x", "y", "z", "n", "cat"))

def prepare_t2kde_Data(
    inFile,
    outFile,
    dtype=np.float32,
    slice=None,
    badEvents = []
):
    """
    This function reads in data in inFile and writes a pair of
    numpy arrays  to the outFile. 

    dtype: Select a different dtype (like float16)
    slice: Allow just a slice of data to be loaded
    """

## these unit vectors will be used to convert the elements of 
## the ellipsoid major and minor axis vectors into vectors
    xhat = np.array([1, 0, 0])
    yhat = np.array([0, 1, 0])
    zhat = np.array([0, 0, 1])

    Xlist = []
    Ylist = []

    Xlist_ints = []
    Ylist_ints = []

    print("Loading data...")

    msg = f"Loaded {inFile} in {{time:.4}} s"
    with Timer(msg), h5py.File(inFile, mode="r") as f:
            ## [:,np.newaxis,:] makes X (a x b) --> (a x 1 x b) (axis 0, axis 1, axis 2)
            ## a is *probably* 4000 and b is *probably* N, but it could be the other
            ## way around;  check iwth .shape

## Here we read in the KDE itself plus the values of x and y where the KDE is maximal for 
## each bin of z. It appears that in the test file the original KDE values .AND. the values 
## of Xmax and Ymax have been divided by 2500. This should have been done only for the 
## KDE values, so Xmax and Ymax are re-scaled to better use the dynamic range available 
## using np.float16

## mds 200729  the KDE targets have many zeros. Learning zeros using a ratio
## mds         of predicted to target means that overestimating by a small
## mds         amount in the cost function, even adding an epsilon-like parameter## mds         there is difficult. Let's explicitly add epsilon here.
## mds         We might be able to do it equally well in the cost function,
## mds         but doing it here makes plotting easy as well.

            epsilon = 0.001 
## mds 201019            k)ernel = np.asarray(f["kernel"]) + epsilon
## we want to use the poca KDE, not the original kernel
            kernel = np.asarray(f["poca_KDE_A"]) + epsilon
            Xmax = 2500.*np.asarray(f["Xmax"])
            Ymax = 2500.*np.asarray(f["Ymax"]) 
            
            Y = ja.concatenate((kernel,Xmax,Ymax),axis=1).astype(dtype_Y)

            print("  ")
            print("kernel.shape = ",kernel.shape)
            print("kernel.shape[0] = ",kernel.shape[0])
            print("kernel.shape[1] = ",kernel.shape[1])
            print("Y.shape =      ",Y.shape)
            nEvts = kernel.shape[0]
            nBins = kernel.shape[1]
            binsPerInterval = int(100)
            nIntervals = int(nBins/binsPerInterval)
            print("binsPerInterval = ",binsPerInterval)
            print("nIntervals =       ",nIntervals)
            if (nBins != (binsPerInterval*nIntervals)):
              print("nBins = ",nBins)
              print("binsPerInterval*nIntervals = ",binsPerInterval*nIntervals)

            intervalKernels = np.reshape(kernel,(nEvts*nIntervals,binsPerInterval))
            intervalXmax    = np.reshape(Xmax,(nEvts*nIntervals,binsPerInterval))
            intervalYmax    = np.reshape(Ymax,(nEvts*nIntervals,binsPerInterval))
## don't want Xmas and Ymax            Y_intervals     = ja.concatenate((intervalKernels,intervalXmax,intervalYmax),axis=1).astype(dtype_Y)
            Y_intervals     = intervalKernels


            print("intervalKernels.shape = ",intervalKernels.shape)


##  code to test that intervalKernels is organized 'as expected'
## mds             for index in range(99):
## mds               print("index = ",index)
## mds               print("kernel[0,index], intervalKernels[0,index], Delta = ", kernel[0,index], intervalKernels[0,index], kernel[0,index]-intervalKernels[0,index])
## mds               print("kernel[0,100+index], intervalKernels[1,index], Delta = ",kernel[0,100+index]-intervalKernels[1,index])
## mds             
## now build the feature set from the relevant tracks' parameters
## we need to use "afile" to account for the variable length
## structure of the awkward arrays

##  201018  use poca ellipsoid parameter rather than "track parameters"
        
            afile = awkward.hdf5(f)

##  220715 remove pocaz scaling here to use raw values in mm
##  we probably want to maintain scales in mm everywhere
##  or consistently rescale all of x,y,z,A,B, etc.            
##            pocaz = np.asarray(0.001*afile["poca_z"].astype(dtype_Y))
            pocaz = np.asarray(afile["poca_z"].astype(dtype_Y))
            pocax = np.asarray(afile["poca_x"].astype(dtype_Y))
            pocay = np.asarray(afile["poca_y"].astype(dtype_Y))
            pocaMx = np.asarray(afile["major_axis_x"].astype(dtype_Y))
            print("pocaMx.shape = ", pocaMx.shape)
            pocaMy = np.asarray(afile["major_axis_y"].astype(dtype_Y))
            pocaMz = np.asarray(afile["major_axis_z"].astype(dtype_Y))
            poca_m1x = np.asarray(afile["minor_axis1_x"].astype(dtype_Y))
            poca_m1y = np.asarray(afile["minor_axis1_y"].astype(dtype_Y))
            poca_m1z = np.asarray(afile["minor_axis1_z"].astype(dtype_Y))
            poca_m2x = np.asarray(afile["minor_axis2_x"].astype(dtype_Y))
            poca_m2y = np.asarray(afile["minor_axis2_y"].astype(dtype_Y))
            poca_m2z = np.asarray(afile["minor_axis2_z"].astype(dtype_Y))

            nEvts = len(pocaz)
            print("nEvts = ", nEvts)
            print("pocaz.shape = ",pocaz.shape)

            print("len(pocaMx[0]) = ", len(pocaMx[0]))
            print("len(pocaMx[1]) = ", len(pocaMx[1]))
            print("len(pocaMx[2]) = ", len(pocaMx[2]))
            print("len(pocaMx[3]) = ", len(pocaMx[3]))
            print("len(pocaMx[4]) = ", len(pocaMx[4]))


##  220817 mds
##  add code to "scrub" poca_ellipsoid data to make sure that when
##  there are illegal values (that can lead to nan results later)
##  they are replaced and the corresponding tracks are "marked"
##  with pocaz values large enough that the tracks will be rejected
##  when IntervalTracks are constructed

            for iEvt in range(nEvts):
                l_pocaz = pocaz[iEvt][:]
                nTrks = l_pocaz.shape[0]
                
##  --    
                if (iEvt < 10):
                  print(" iEvt, nTrks = ", iEvt, nTrks)
##  --
            
                l_pocax = pocax[iEvt][:]
                l_pocay = pocay[iEvt][:]
                
                l_pocaMx = pocaMx[iEvt][:]   
                l_pocaMy = pocaMy[iEvt][:]
                l_pocaMz = pocaMz[iEvt][:]
                
                l_poca_m1x = poca_m1x[iEvt][:]   
                l_poca_m1y = poca_m1y[iEvt][:]
                l_poca_m1z = poca_m1z[iEvt][:]
                
                l_poca_m2x = poca_m2x[iEvt][:]   
                l_poca_m2y = poca_m2y[iEvt][:]
                l_poca_m2z = poca_m2z[iEvt][:]
                
                
                mag_1_sq = np.multiply(l_poca_m1x,l_poca_m1x)
                mag_1_sq = mag_1_sq + np.multiply(l_poca_m1y,l_poca_m1y)
                mag_1_sq = mag_1_sq + np.multiply(l_poca_m1z,l_poca_m1z)
                mag1     = np.sqrt(mag_1_sq)
                    
                mag_2_sq = np.multiply(l_poca_m2x,l_poca_m2x)
                mag_2_sq = mag_1_sq + np.multiply(l_poca_m2y,l_poca_m2y)
                mag_2_sq = mag_1_sq + np.multiply(l_poca_m2z,l_poca_m2z)
                mag2     = np.sqrt(mag_2_sq)
                
##  --    
                if (iEvt < 0):
                    maxTrk = min(5,nTrks)
                    for iTrk in range(maxTrk):
                        print(" iEvt, iTrk = ", iEvt, iTrk)
                        print("l_poca_m1(x,y,z)[iTrk], mag1 = ",l_poca_m1x[iTrk],l_poca_m1y[iTrk],l_poca_m1y[iTrk],mag1[iTrk])
##  --
                                            
                for iTrk in range(nTrks):
                    good_pocaMx = math.isfinite(l_pocaMx[iTrk])
                    bad_pocaMx = not good_pocaMx
                    good_pocaMy = math.isfinite(l_pocaMy[iTrk])
                    bad_pocaMy = not good_pocaMy
                    good_pocaMz = math.isfinite(l_pocaMz[iTrk])
                    bad_pocaMz = not good_pocaMz
                    if (mag1[iTrk]<1e-10 or mag2[iTrk]<1e-10 or
                        bad_pocaMx or bad_pocaMy or bad_pocaMz) :
                        print(" BAD ---- iEvt, iTrk = ",iEvt,iTrk)
## mds 220826                        print("l_pocaMx[iTrk] = ",l_pocaMx[iTrk])
## mds 220826                        print("l_pocaMy[iTrk] = ",l_pocaMy[iTrk])
## mds 220826                        print("l_pocaMz[iTrk] = ",l_pocaMz[iTrk])
## mds 220826                        print("l_poca_m1x[iTrk] = ",l_poca_m1x[iTrk])
## mds 220826                        print("l_poca_m1y[iTrk] = ",l_poca_m1y[iTrk])
## mds 220826                        print("l_poca_m1z[iTrk] = ",l_poca_m1z[iTrk])
## mds 220826                        print("l_poca_m2x[iTrk] = ",l_poca_m2x[iTrk])
## mds 220826                        print("l_poca_m2y[iTrk] = ",l_poca_m2y[iTrk])
## mds 220826                        print("l_poca_m2z[iTrk] = ",l_poca_m2z[iTrk])
                        
                        
## if there is a problem, over-write the error ellipsoid values
## with bogus (but finite & orthogonal) values that will flag
## the later code to ignore this track
                        pocaMx[iEvt][iTrk] = 0.0
                        pocaMy[iEvt][iTrk] = 0.0
                        pocaMz[iEvt][iTrk] = 100.
                        
                        poca_m1x[iEvt][iTrk] = 1.0
                        poca_m1y[iEvt][iTrk] = 0.0
                        poca_m1z[iEvt][iTrk] = 0.0
                        
                        poca_m2x[iEvt][iTrk] = 0.0
                        poca_m2y[iEvt][iTrk] = 1.0
                        poca_m2z[iEvt][iTrk] = 0.0

##  end of scrubbing code


            Mx = np.multiply(pocaMx.reshape(nEvts,1),xhat)
            My = np.multiply(pocaMy.reshape(nEvts,1),yhat)
            Mz = np.multiply(pocaMz.reshape(nEvts,1),zhat)
            majorAxis = Mx+My+Mz
            print("majorAxis.shape = ",majorAxis.shape)


            mx = np.multiply(poca_m1x.reshape(nEvts,1),xhat)
            my = np.multiply(poca_m1y.reshape(nEvts,1),yhat)
            mz = np.multiply(poca_m1z.reshape(nEvts,1),zhat)
            minorAxis_1 = mx+my+mz
            print("minorAxis_1.shape = ",minorAxis_1.shape)

            mx = np.multiply(poca_m2x.reshape(nEvts,1),xhat)
            my = np.multiply(poca_m2y.reshape(nEvts,1),yhat)
            mz = np.multiply(poca_m2z.reshape(nEvts,1),zhat)
            minorAxis_2 = mx+my+mz
            print("minorAxis_2.shape = ",minorAxis_1.shape)


            A, B, C, D, E, F = six_ellipsoid_parameters(majorAxis,minorAxis_1,minorAxis_2)

            print("A.shape = ",A.shape)
            for iTrk in range(2):
              print("majorAxis[iTrk][0][0] = ",majorAxis[iTrk][0][0])
              print("majorAxis[iTrk][1][0] = ",majorAxis[iTrk][1][0])
              print("majorAxis[iTrk][2][0] = ",majorAxis[iTrk][2][0])
              print("minorAxis_1[iTrk][0][0] = ",minorAxis_1[iTrk][0][0])
              print("minorAxis_1[iTrk][1][0] = ",minorAxis_1[iTrk][1][0])
              print("minorAxis_1[iTrk][2][0] = ",minorAxis_1[iTrk][2][0])
              print("minorAxis_2[iTrk][0][0] = ",minorAxis_2[iTrk][0][0])
              print("minorAxis_2[iTrk][1][0] = ",minorAxis_2[iTrk][1][0])
              print("minorAxis_2[iTrk][2][0] = ",minorAxis_2[iTrk][2][0])
              print("  ")
## mdsAA              print("A[iTrk][0] = ",A[iTrk][0])
## mdsAA              print("B[iTrk][0] = ",B[iTrk][0])
## mdsAA              print("C[iTrk][0] = ",C[iTrk][0])
## mdsAA              print("D[iTrk][0] = ",D[iTrk][0])
## mdsAA              print("E[iTrk][0] = ",E[iTrk][0])
## mdsAA              print("F[iTrk][0] = ",F[iTrk][0])
## mds              print("majorAxis[iTrk][0] = ", majorAxis[iTrk][0])
## mds              print("majorAxis[iTrk][1] = ", majorAxis[iTrk][1])
## mds              print("majorAxis[iTrk][2] = ", majorAxis[iTrk][2])


            


## add some "debugging" code to make sure I understand enumerate
##  mds 220711

            minZ = -100.
            maxZ =  300.
            intervalLength = (maxZ-minZ)/nIntervals
            print(" *** intervalLength = ",intervalLength,"   ***")

##  mark non-track data with -99 as a flag
## mds 220821            maxIntLen = 150  ## to be re-visited  mds 220712
            maxIntLen = 250  ## increased as some intervals clearly have more than 200 tracks
            padded_int_pocaz   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocax   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocay   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaA   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaB   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaC   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaD   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaE   = np.zeros((nEvts*nIntervals,maxIntLen))-99.
            padded_int_pocaF   = np.zeros((nEvts*nIntervals,maxIntLen))-99.

            for  eventIndex, e in enumerate(pocaz):
              if (eventIndex<1):
                print("eventIndex = ",eventIndex)
              local_pocaz = pocaz[eventIndex][:]
              local_pocax = pocax[eventIndex][:]
              local_pocay = pocay[eventIndex][:]
              local_A = A[eventIndex][:]
              local_B = B[eventIndex][:]
              local_C = C[eventIndex][:]
              local_D = D[eventIndex][:]
              local_E = E[eventIndex][:]
              local_F = F[eventIndex][:]
  
              indices = np.argsort(local_pocaz)

              ordered_pocaz = local_pocaz[indices]
              ordered_pocax = local_pocax[indices]
              ordered_pocay = local_pocay[indices]
              ordered_A     = local_A[indices]
              ordered_B     = local_B[indices]
              ordered_C     = local_C[indices]
              ordered_D     = local_D[indices]
              ordered_E     = local_E[indices]
              ordered_F     = local_F[indices]
  
              if (eventIndex<0): 
                print("len(local_pocaz) = ",len(local_pocaz))
                print("  ")
                print("local_pocaz = ",local_pocaz)
                print("ordered_pocaz = ",ordered_pocaz) 
                print("      -----------      ")
                print("local_pocax = ",local_pocax)
                print("ordered_pocax = ",ordered_pocax)
                print("  ---------------------- \n")

              for interval in range(nIntervals):
                interval_lowEdge  = minZ + interval*intervalLength
                interval_highEdge = interval_lowEdge + intervalLength 
                interval_minZ     = interval_lowEdge - 2.5
                interval_maxZ     = interval_highEdge + 2.5
                if (eventIndex<1):
                    print(" -- interval, interval_minZ, interval_maxZ = ",interval, interval_minZ, interval_maxZ)
                if (2460==eventIndex):
                    intervalRange = (ordered_pocaz>99999.) ## ugly way to remove all poca-ellipsoids in this event
                else:
                    intervalRange = (ordered_pocaz>interval_minZ) & (ordered_pocaz<interval_maxZ)
## for each interval we want the values of z shifted to be centered at the
## center of the interval
                interval_pocaz = ordered_pocaz[intervalRange] - interval_lowEdge
                interval_pocax = ordered_pocax[intervalRange]
                interval_pocay = ordered_pocay[intervalRange]
                interval_A     = ordered_A[intervalRange]
                interval_B     = ordered_B[intervalRange]
                interval_C     = ordered_C[intervalRange]
                interval_D     = ordered_D[intervalRange]
                interval_E     = ordered_E[intervalRange]
                interval_F     = ordered_F[intervalRange]

                intervalSigmaZ = np.sqrt(np.divide(1.,interval_C))
                intervalSigmaX = np.sqrt(np.divide(1.,interval_A))
                intervalSigmaY = np.sqrt(np.divide(1.,interval_B))
                xSigmas = np.divide(interval_pocax,intervalSigmaX)
                ySigmas = np.divide(interval_pocay,intervalSigmaY)

                veryGoodTracks =  (intervalSigmaZ<2.0) & (np.absolute(xSigmas)<4.0) & (np.absolute(ySigmas)<4.0)
               
                interval_pocaz = interval_pocaz[veryGoodTracks]
                interval_pocax = interval_pocax[veryGoodTracks]
                interval_pocay = interval_pocay[veryGoodTracks]
                interval_A     = interval_A[veryGoodTracks]
                interval_B     = interval_B[veryGoodTracks]
                interval_C     = interval_C[veryGoodTracks]
                interval_D     = interval_D[veryGoodTracks]
                interval_E     = interval_E[veryGoodTracks]
                interval_F     = interval_F[veryGoodTracks]

                if (eventIndex<0): 
                    print("  ")
                    if (interval<5):
                      print("eventIndex, interval = ",eventIndex, interval)
                      print("interval_pocaz = ",interval_pocaz)
                      print("             ----          ")
                      print("interval_pocax = ",interval_pocax)

## and now for all intervals for the eventIndex range
                    print("  ")
                    print("eventIndex and interval = ",eventIndex,interval) 
                    print("interval_pocaz = ",interval_pocaz)
                fillingLength = min(len(interval_pocaz),maxIntLen)
                ii = eventIndex*nIntervals + interval
                padded_int_pocaz[ii,:fillingLength] = interval_pocaz[:fillingLength].astype(dtype_Y)
                padded_int_pocax[ii,:fillingLength] = interval_pocax[:fillingLength].astype(dtype_Y)
                padded_int_pocay[ii,:fillingLength] = interval_pocay[:fillingLength].astype(dtype_Y)
                padded_int_pocaA[ii,:fillingLength] = interval_A[:fillingLength].astype(dtype_Y)
                padded_int_pocaB[ii,:fillingLength] = interval_B[:fillingLength].astype(dtype_Y)
                padded_int_pocaC[ii,:fillingLength] = interval_C[:fillingLength].astype(dtype_Y)
                padded_int_pocaD[ii,:fillingLength] = interval_D[:fillingLength].astype(dtype_Y)
                padded_int_pocaE[ii,:fillingLength] = interval_E[:fillingLength].astype(dtype_Y)
                padded_int_pocaF[ii,:fillingLength] = interval_F[:fillingLength].astype(dtype_Y)

################                

            padded_int_pocaz  = padded_int_pocaz[:,np.newaxis,:]
            padded_int_pocax  = padded_int_pocax[:,np.newaxis,:]
            padded_int_pocay  = padded_int_pocay[:,np.newaxis,:]
            padded_int_pocaA  = padded_int_pocaA[:,np.newaxis,:]
            padded_int_pocaB  = padded_int_pocaB[:,np.newaxis,:]
            padded_int_pocaC  = padded_int_pocaC[:,np.newaxis,:]
            padded_int_pocaD  = padded_int_pocaD[:,np.newaxis,:]
            padded_int_pocaE  = padded_int_pocaE[:,np.newaxis,:]
            padded_int_pocaF  = padded_int_pocaF[:,np.newaxis,:]


            X_ints = ja.concatenate((padded_int_pocaz,padded_int_pocax,padded_int_pocay,padded_int_pocaA,padded_int_pocaB,padded_int_pocaC,padded_int_pocaD,padded_int_pocaE,padded_int_pocaF),axis=1).astype(dtype_X)

            print("len(X_ints) =",len(X_ints))

            Xlist_ints.append(X_ints)
            Ylist_ints.append(Y_intervals)

            print("len(Xlist_ints) = ",len(Xlist_ints))

    X_intervals = np.concatenate(Xlist_ints, axis = 0)
    Y_intervals = np.concatenate(Ylist_ints, axis = 0)

    print(" ")
    print("  -------  ")
    print("X_intervals.shape = ",X_intervals.shape)
    print("Y_intervals.shape = ",Y_intervals.shape)

    intervalsPerEvent = nIntervals
    print("nEvts, nIntervals, intervalsPerEvent = ", nEvts, nIntervals, intervalsPerEvent)

    badIntervals = np.asarray([],dtype=int)
    for iEvt in badEvents:
        badInts = np.arange(iEvt*intervalsPerEvent,iEvt*intervalsPerEvent+intervalsPerEvent)
        badIntervals = np.concatenate((badIntervals,badInts),axis=0)
        
## mds    print("badIntervals = ",badIntervals)

    X_intervals = np.delete(X_intervals,badIntervals,axis=0)
    Y_intervals = np.delete(Y_intervals,badIntervals,axis=0)

    print("  ------- after dropping bad events' intervals ---  ")
    print("X_intervals.shape = ",X_intervals.shape)
    print("Y_intervals.shape = ",Y_intervals.shape)
    print("  ")
    if slice:

        X_intervals = X_intervals[slice, :]
        Y_intervals = Y_intervals[slice, :]

    with open(outFile, 'wb') as f:
        np.save(f,X_intervals)
        np.save(f,Y_intervals)
    
    return 

####### -----------------
