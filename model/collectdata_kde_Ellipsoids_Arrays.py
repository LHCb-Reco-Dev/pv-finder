##
## mds September 3, 2022
##
## this code is designed to read a .npy file with two sets of arrays,
## X and Y corresponding to features and labels that will
## be ussed to construct the tensors from which Pytorch DataSets
## and then a DataLoader thatis returned.
##

import torch
from torch.utils.data import TensorDataset

import numpy as np
from pathlib import Path
from functools import partial
import warnings
from collections import namedtuple

from .utilities import Timer
from .jagged import concatenate




# This can throw a warning about float - let's hide it for now.
with warnings.catch_warnings():
    warnings.simplefilter("ignore", category=FutureWarning)
    import h5py

try:
    import awkward0 as awkward
except ModuleNotFoundError:
    import awkward

ja = awkward.JaggedArray


def collect_t2kde_arrays(
    *files,
    batch_size=1,
    dtype=np.float32,
    device=None,
    slice=None,
    **kargs,
):
    """
    This function collects arrays. It does not split it up. You can pass in multiple files.
    Example: collect_data('a.h5', 'b.h5')

    batch_size: The number of events per batch
    dtype: Select a different dtype (like float16)
    slice: Allow just a slice of data to be loaded
    device: The device to load onto (CPU by default)
    **kargs: Any other keyword arguments will be passed on to torch's DataLoader
    """

    Xlist = []
    Ylist = []

    for XY_file in files:
        print("XY_file = ",XY_file)
        msg = f"Loaded {XY_file} in {{time:.4}} s"
        with Timer(msg), open(XY_file, "rb") as f:
            X_in = np.load(f)
            Y_in = np.load(f)
            Xlist.append(X_in)
            Ylist.append(Y_in)

    X = np.concatenate(Xlist, axis = 0)
    Y = np.concatenate(Ylist, axis = 0)
    print("outer loop X.shape = ", X.shape)

    if slice:
        X = X[slice, :]
        Y = Y[slice, :]


    with Timer(start=f"Constructing {X.shape[0]} event dataset"):
        x_t = torch.tensor(X)
        y_t = torch.tensor(Y)

        if device is not None:
            x_t = x_t.to(device)
            y_t = y_t.to(device)

        dataset = TensorDataset(x_t, y_t)

    loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, **kargs)
    print("x_t.shape = ",x_t.shape)
    print("x_t.shape[0] = ", x_t.shape[0])
    print("x_t.shape[1] = ", x_t.shape[1])

    print("y_t.shape = ",y_t.shape)
    
    return loader

####### -----------------
