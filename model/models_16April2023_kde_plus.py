## started with HDplusUNet100A in models_July2022_kde_plus.py
## this code is meant to "unwrap" Will's functions that nest
## the batch normalizations, which appears to prevent the
## autocast Automatic Mixed Precision method from working correctly.


##    ---------------------

## derived from TrackIntervalsToKDE_HDplusUNet100 
##  mds 220925 --  this variation uses symmetric
##  contractions and expansions (not done in UNet100
##  by mistake) and it mimics the original UNet paper
##  (https://arxiv.org/abs/1505.04597) by increasing 
##  the number of channels by a factor of 2 in each
##  contraction and increasing again upon expansion.
##  the rcbn layers are given new labels so we can
##  start from an original HDplusUNet100 model and
##  freeze the earlier weights  


import torch.nn as nn
import torch.nn.functional as F
import torch

from functools import partial

## ----------------------

class TrackIntervalsToKDE_HDplusUNet100A(nn.Module):
    softplus = torch.nn.Softplus()

    def __init__(self, nOut1=25, nOut2=25, nOut3=25,
                       nOut4=25, nOut5=25,
                       latentChannels=8,
                       n=64,
                       sc_mode='concat',
                       dropout_p=.25,
                       d_selection='ConvBNrelu',
                       u_selection='Up'
                       ):
        super(TrackIntervalsToKDE_HDplusUNet100A,self).__init__()

        self.nOut1 = nOut1
        self.nOut2 = nOut2
        self.nOut3 = nOut3
        self.nOut4 = nOut4
        self.nOut5 = nOut5

        self.latentChannels = latentChannels
       
####--------------- old HDplus code

        self.layer1 = nn.Linear(
                    in_features = 9,
                    out_features = self.nOut1,
                    bias = True)
        self.layer2 = nn.Linear(
                    in_features = self.layer1.out_features,
                    out_features = self.nOut2,
                    bias = True)
        self.layer3 = nn.Linear(
                    in_features = self.layer2.out_features,
                    out_features = self.nOut3,
                    bias = True)
        self.layer4 = nn.Linear(
                    in_features = self.layer3.out_features,
                    out_features = self.nOut4,
                    bias = True)
        self.layer5 = nn.Linear(
                    in_features = self.layer4.out_features,
                    out_features = self.nOut5,
                    bias = True)

## replace 4000 bin predicted features to be 100 for looking at 100-bin intervals
## mds 220714
        self.layer6A = nn.Linear(
                    in_features = self.layer5.out_features,
                    out_features = latentChannels*100,
                    bias = True)

        
# ======================================================================
#                            U-Net Models
# ======================================================================

## 220728  derived from Will's UNet model, but modified for use in 
##         tracks-to-hists architecture

##  mds 
##  mds  Here is the link to the original U-Net paper: https://arxiv.org/abs/1505.04597
##  mds '''

## if we are doing "concatenation" skip connections, an output dimension of 16 during 
## encoding will lead to an input dim of 32 during decoding.
## however, if we add instead of concatenate, we will have an input dimension of 16 
## during encoding AND decoding, so we do not need to scale up at all.
## We will multiply by this factor when we define our decoder, to make sure the shapes match up.
        if sc_mode == 'concat':
            factor = 2
        else:
            factor = 1
        self.mode = sc_mode
        self.p = dropout_p
        self.convDropout = nn.Dropout(self.p)
        self.relu = nn.ReLU()
        self.bn1 = nn.BatchNorm1d(n)

        self.downConv_L1 = nn.Conv1d(
            in_channels = self.latentChannels,
            out_channels = n,
            kernel_size = 15,
            padding = (15-1) // 2,
        )

        self.bn1 = nn.BatchNorm1d(n)

        self.downConv_L2 = nn.Conv1d(
            in_channels = n,
            out_channels = 2*n,
            kernel_size = 7,
            padding = (7-1) // 2,
        )

        self.bn2 = nn.BatchNorm1d(2*n)

        self.downConv_L3 = nn.Conv1d(
            in_channels =  2*n,
            out_channels = 4*n,
            kernel_size = 5,
            padding = (5-1) // 2,
        )

        self.bn3 = nn.BatchNorm1d(4*n)


##  functions to mimic elements of Will's
##  class Up(nn.Sequential):
##      """transpose convolution => convolution => [BN] => ReLU"""
##      def __init__(self, in_channels, out_channels, kernel_size=3, p=0):
##          super().__init__(
##              nn.ConvTranspose1d(in_channels, out_channels, 2, 2),
##              ConvBNrelu(out_channels, out_channels, kernel_size=kernel_size, p=p))


        self.transpose_L1 = nn.ConvTranspose1d(
            in_channels  = 4*n,
            out_channels = 2*n,
            kernel_size  = 2,
            stride       = 2)


        self.upConv_L1 = nn.Conv1d(
            in_channels  = 2*n,
            out_channels = 2*n,
            kernel_size = 5,
            padding = (5-1) // 2,
        )

        self.bn1up = nn.BatchNorm1d(2*n)

        self.transpose_L2 = nn.ConvTranspose1d(
            in_channels  = 2*n*factor,
            out_channels = n,
            kernel_size  = 2,
            stride       = 2)


        self.upConv_L2 = nn.Conv1d(
            in_channels  = n,
            out_channels = n,
            kernel_size = 5,
            padding = (5-1) // 2,
        )

        self.bn2up = nn.BatchNorm1d(n)

        self.out_intermediateUp = nn.Conv1d(n*factor, n, 5, padding=2)

        self.outC = nn.Conv1d(n, 1, 5, padding=2) # we need to project the n-dimensional output channels down to one, so we can call .squeeze() to remove it

        self.d = nn.MaxPool1d(2)



########  end of 230416  additions
    def forward(self, x):
        
## mds        print("in forward, x.shape = ",x.shape)
        leaky = nn.LeakyReLU(0.01)
        
        nEvts     = x.shape[0]
        nFeatures = x.shape[1]
        nTrks     = x.shape[2]
## mds        print("nEvts = ", nEvts,"   nFeatures = ", nFeatures, "  nTrks = ", nTrks)
        mask = x[:,0,:] > -98.
        filt = mask.float()
        f1 = filt.unsqueeze(2)
## mds 220714; again 4000 --> 100 when trying to predict intervals of 100 bins at a time
##        f2 = f1.expand(-1,-1,4000)
        f2 = f1.expand(-1,-1,100)
##        print("filt.shape = ",filt.shape)
##        print("f1.shape = ",f1.shape, "f2.shape = ",f2.shape)
        x = x.transpose(1,2)
##        print("after transpose, x.shape = ", x.shape)
        ones = torch.ones(nEvts,nFeatures,nTrks)
      
## make a copy of the initial features so they can be passed along using a skip connection 
        x0 = x 
        x = leaky(self.layer1(x))
        x = leaky(self.layer2(x))
        x = leaky(self.layer3(x))
        x = leaky(self.layer4(x))
        x = leaky(self.layer5(x))
        x = leaky(self.layer6A(x))  ## produces latentChannels x 100 bins for intervals
##        print(' at point Aa, x.shape = ',x.shape)
       
        x = x.view(nEvts,nTrks,self.latentChannels,100)
##        print(' at point AA, x.shape = ',x.shape)

## here we are summing over all the tracks, creating "y"
## which has a sum of all tracks' contributions in each of
## latentChannels for each event and each bin of the (eventual)
## KDE histogram
##        print("before unsqueezing, f2.shape = ",f2.shape)
        f2 = torch.unsqueeze(f2,2)
##        print("x.shape = ",x.shape)
##        print("after unsqueezing,  f2 = torch.unsqueeze(f2,2), f2,shape = ",f2.shape)
        x = torch.mul(f2,x)
        y0 = torch.sum(x,dim=1)
## ##      print(' at point AB, y0.shape = ',y0.shape)

##   code for execution of UNet100 model

## the comments that follow some lines are the output sizes;
## the input size for lhcb is 4000
## for the initial studies, the interval size is 100 bins

######  -- about to insert

##      print("y0.shape = ",y0.shape)
## ----  moral equivalent of x1 = self.rcbnA1(y0)

        x1 = self.downConv_L1(y0)
        x1 = self.relu(self.bn1(x1))
        x1 = self.convDropout(x1)  # 100
##      print(" x1.shape = ",x1.shape)

## ---- moral equivalent of x2 = self.d(self.rcbnA2(x1))

        x2 = self.downConv_L2(x1)
##      print(' x2,shape after downConv_L2 = ',x2.shape)
        x2 = self.relu(self.bn2(x2))
        x2 = self.convDropout(x2)
        x2 = self.d(x2)            # 50
##      print("  x2.shape after self.d = ",x2.shape)

## ---- moral equivalent of x = self.d(self.rcbnA3(x2))

        x3 = self.downConv_L3(x2)
        x3 = self.relu(self.bn3(x3))
        x3 = self.convDropout(x3)
        x3 = self.d(x3)           #25
##      print("   x3.shape    " ,x3.shape)


######  -- end of insert

## ---- moral equivalent of  x = self.up1A(x)

        x  = self.transpose_L1(x3)
##      print('x.shape after transpose_L1(x3) = ',x.shape)
        x  = self.upConv_L1(x)
##      print('  x.shape after upConv_L1(x) = ',x.shape)
        x  = self.relu(self.bn1up(x))
        x  = self.convDropout(x)     #50
        
## mds  print("after up1A, x.shape = ",x.shape)

## ----  moral equivalent of x = self.up2A(combine(x, x2, mode=self.mode))
        x  = combine(x, x2, mode=self.mode)
##      print('    x.shape after combine(x, x2, mode=self.mode) = ',x.shape)

        x  = self.transpose_L2(x)
        x  = self.upConv_L2(x)
##      print('     x.shape after upConv_L2(x) = ',x.shape)
        x  = self.relu(self.bn2up(x))
        x  = self.convDropout(x)


## ----  moral equivalent of x = combine(x, x1, mode=self.mode)
        x = combine(x, x1, mode=self.mode)
##      print('     x.shape after combine(x, x1, mode=self.mode) = ',x.shape)

## ---- moral equivalent of x = self.out_intermediateA(x) # 100
        x = self.out_intermediateUp(x)

## ---- moral equivalent of logits_x0 = self.outcA(x)
        logits_x0 = self.outC(x)

## ---- moral equivalent of y_prime = F.softplus(logits_x0).squeeze()
        y_prime = F.softplus(logits_x0).squeeze() # squeeze removes empty dimensions.. (64, 1, 100) -> (64, 100)



        y_pred = torch.mul(y_prime,0.001)
        return y_pred

####### -------------
def combine(x, y, mode='concat'):
    if mode == 'concat':
        return torch.cat([x, y], dim=1)
    elif mode == 'add':
        return x+y
    else:
        raise RuntimeError(f'''Invalid option {mode} from choices 'concat' or 'add' ''')

# ======================================================================
