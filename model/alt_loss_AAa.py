import torch
## alt_Loss_AAa.py derived from alt_Loss_AA.py 230414
## modified "valid" to include both predicted and target values not nan

##  derived from alt_loss_A.py used for KDE_to_hists learning
##  mds 220716
##  modify to normalize to number of events in bactch and
##  number of bins (for use with intervals tracks_to_hists)

# how to define our cost function
class Loss(torch.nn.Module):
    def __init__(self, epsilon=1e-5, coefficient=5.0):
        """
        Epsilon is a parameter that can be adjusted.
        """

        # You must call the original constructor (torch.nn.Module.__init__(self))!
        super().__init__()

        # Now you can add things
        self.epsilon = epsilon
        self.coefficient = coefficient
        self.iteration = 0

    def forward(self, x, y):
        # Make a boolean mask of non-nan values of the target histogram.
        # This will be used to select items from y:
        # see https://docs.scipy.org/doc/numpy-1.13.0/user/basics.indexing.html#boolean-or-mask-index-arrays
        #
        # Note that if masking was not requested when loading the data, there
        # will be no NaNs and this will be all Trues and will do nothing special.
        nBatches       = y.shape[0]
        nBinsPerBatch = y.shape[1]

##        print("in alt_loss_AA, nBatches, nBinsPerBatch = ",nBatches, nBinsPerBatch)

##  add masking for nan values of x; this could be dangerous; may be OK if rare enough.
        valid_y = ~torch.isnan(y)
        valid_x = ~torch.isnan(x)
        valid   = torch.logical_and(valid_y,valid_x)

        # Compute r, only including non-nan values. r will probably be shorter than x and y.
        r = torch.abs((x[valid] + self.epsilon) / (y[valid] + self.epsilon))

        # Compute -log(2r/(r² + 1))
        alpha = -torch.log(2 * r / (r ** 2 + 1))
        alpha = alpha * (1.0 + self.coefficient * torch.exp(-r))

        # Sum up the alpha values, and divide by the length of x and y. Note this is not quite
        # a .mean(), since alpha can be a bit shorter than x and y due to masking.
##        beta = alpha.sum() / 4000

##     in alt_loss_AA, change the normalization to (roughly) correspond to an
##     average loss-per-bin value (ignoring masked bins)
        beta = alpha.sum() / (nBatches*nBinsPerBatch)

##        print(f"in Loss, iteration ={self.iteration}",f"  and beta = {beta}")
##        self.iteration += 1

        return beta
