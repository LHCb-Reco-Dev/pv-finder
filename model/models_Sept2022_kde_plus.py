## 220903  copy from models_Sept2022_kde_plus.py
## first modification is adding dropout after 6th fully connected layer 
## in TrackIntervalsToKDE_HDplusUNet100
## When starting with weights trained using HDplus, the training cost
## improves steadily while the validation loss increases.  This
## strong suggests too much "capacity" is leading to over-training.
## FWIW, the training cost was below 0.8 for both training and 
## validation with just HD model; slightly better with HDplus;
## current HDplus_UNet100 has training below 0.8 but validation
## cost increased from ~ 3 to > 5 



##  220714  mds, derived from the  original DirtyDozen model in models_kde.py

##  This is a first model to predict KDEs from poca_ellipsoids using
##  interals of 100 bins at a time.

##  For the moment, the goal is to predict the central KDE values only,
##  not the corresponding Xmax and Ymax values.

##  An interesting question, if this works well, is whether we can simply
##  replace learning the KDE with learning the target histograms themselves.



import torch.nn as nn
import torch.nn.functional as F
import torch

from functools import partial


##  mds 220728  add classes/methods needed to build a UNet model for the
##  "second segment" that produces target histograms as the output

class ConvBNrelu(nn.Sequential):
    """convolution => [BN] => ReLU

    This class simply combines a few layers into a "block", which will be very commonly used throughout multiple different models.
    You can specify the parameters of the conv layer when you initialize, and the rest will be automatically sorted out.
    """
    def __init__(self, in_channels, out_channels, kernel_size=3, p=0):
        super(ConvBNrelu, self).__init__(
        nn.Conv1d(in_channels, out_channels, kernel_size, stride=1, padding=(kernel_size-1)//2),
        nn.BatchNorm1d(out_channels),
        nn.ReLU(),
        nn.Dropout(p)
#         Swish_module(),
)

class Up(nn.Sequential):
    """transpose convolution => convolution => [BN] => ReLU"""
    def __init__(self, in_channels, out_channels, kernel_size=3, p=0):
        super().__init__(
            nn.ConvTranspose1d(in_channels, out_channels, 2, 2),
            ConvBNrelu(out_channels, out_channels, kernel_size=kernel_size, p=p))






# These functions were defined so that you can specify components of a U-Net network without importing new objects/classes
# With these, you can just  use strings to specify what you want. This would be useful if we ever do a grid search over
# different model architectures, or move to AWS where we want to configure architecture without writing any new code.
# ======================================================================
downsample_options = {
    'ConvBNrelu':ConvBNrelu,
}

upsample_options = {
    'Up':Up,
}

def combine(x, y, mode='concat'):
    if mode == 'concat':
        return torch.cat([x, y], dim=1)
    elif mode == 'add':
        return x+y
    else:
        raise RuntimeError(f'''Invalid option {mode} from choices 'concat' or 'add' ''')

# ======================================================================



class TrackIntervalsToKDE_DirtyDozen(nn.Module):
    softplus = torch.nn.Softplus()

    def __init__(self, nOut1=25, nOut2=25, nOut3=25,
                       nOut4=25, nOut5=25, nOut6=50,
                       nOut7=25, nOut8=25, nOut9=50,
                       nOut10=25, nOut11=25):
        super(TrackIntervalsToKDE_DirtyDozen,self).__init__()

        self.nOut1 = nOut1
        self.nOut2 = nOut2
        self.nOut3 = nOut3
        self.nOut4 = nOut4
        self.nOut5 = nOut5
        self.nOut6 = nOut6
        self.nOut7 = nOut7
        self.nOut8 = nOut8
        self.nOut9 = nOut9
        self.nOut10 = nOut10
        self.nOut11 = nOut11
       

        self.layer1 = nn.Linear(
                    in_features = 9,
                    out_features = self.nOut1,
                    bias = True)
        self.layer2 = nn.Linear(
                    in_features = self.layer1.out_features,
                    out_features = self.nOut2,
                    bias = True)
        self.layer3 = nn.Linear(
                    in_features = self.layer2.out_features,
                    out_features = self.nOut3,
                    bias = True)
        self.layer4 = nn.Linear(
                    in_features = self.layer3.out_features,
                    out_features = self.nOut4,
                    bias = True)
        self.layer5 = nn.Linear(
                    in_features = self.layer4.out_features,
                    out_features = self.nOut5,
                    bias = True)
        self.layer6 = nn.Linear(
                    in_features = self.layer5.out_features,
                    out_features = self.nOut6,
                    bias = True)
        self.layer7 = nn.Linear(
                    in_features = self.layer6.out_features,
                    out_features = self.nOut7,
                    bias = True)
        self.layer8 = nn.Linear(
                    in_features = self.layer7.out_features,
                    out_features = self.nOut8,
                    bias = True)
        self.layer9 = nn.Linear(
                    in_features = self.layer8.out_features,
                    out_features = self.nOut9,
                    bias = True)
        self.layer10 = nn.Linear(
                    in_features = self.layer9.out_features,
                    out_features = self.nOut10,
                    bias = True)
        self.layer11 = nn.Linear(
                    in_features = self.layer10.out_features,
                    out_features = self.nOut11,
                    bias = True)

## replace 4000 bin predicted features to be 100 for looking at 100-bin intervals
## mds 220714
        self.layer12 = nn.Linear(
                    in_features = self.layer11.out_features,
                    out_features = 100,
                    bias = True)
        

        
    def forward(self, x):
        
## mds        print("in forward, x.shape = ",x.shape)
        leaky = nn.LeakyReLU(0.01)
        
        nEvts     = x.shape[0]
        nFeatures = x.shape[1]
        nTrks     = x.shape[2]
## mds        print("nEvts = ", nEvts,"   nFeatures = ", nFeatures, "  nTrks = ", nTrks)
        mask = x[:,0,:] > -98.
        filt = mask.float()
        f1 = filt.unsqueeze(2)
## mds 220714; again 4000 --> 100 when trying to predict intervals of 100 bins at a time
##        f2 = f1.expand(-1,-1,4000)
        f2 = f1.expand(-1,-1,100)
## mds         print("filt.shape = ",filt.shape)
## mds         print("f1.shape = ",f1.shape, "f2.shape = ",f2.shape)
        x = x.transpose(1,2)
## mds        print("after transpose, x.shape = ", x.shape)
        ones = torch.ones(nEvts,nFeatures,nTrks)
      
## make a copy of the initial features so they can be passed along using a skip connection 
        x0 = x 
        x = leaky(self.layer1(x))
        x = leaky(self.layer2(x))
        x = leaky(self.layer3(x))
        x = leaky(self.layer4(x))
        x = leaky(self.layer5(x))
        x = leaky(self.layer6(x))
        x = leaky(self.layer7(x))
        x = leaky(self.layer8(x))
        x = leaky(self.layer9(x))
        x = leaky(self.layer10(x))
        x = leaky(self.layer11(x))
        x = (self.layer12(x))  ## produces 4000 bin feature --> 100 bins for intervals
        x = self.softplus(x)
       
## mds         print("after softplus, x.shape = ",x.shape)

## 220714 mds  4000 --> 100 for 100 bin intervals 
##        x.view(nEvts,-1,4000)
        x.view(nEvts,-1,100)
        y_pred = torch.sum(x,dim=1)
## mds         print("y_pred.shape = ",y_pred.shape)

        x1 = torch.mul(f2,x)
## mds        print("x1.shape = ",x1.shape)

## 220714 mds  4000 --> 100 for 100 bin intervals
##        x1.view(nEvts,-1,4000)
        x1.view(nEvts,-1,100)
        y_prime = torch.sum(x1,dim=1)


## mds        print("y_prime.shape = ",y_prime.shape)
       
## mds        print("y_pred[:,0:10] =  ",y_pred[:,0:10])
## mds        print("y_prime[:,0:10] =  ",y_prime[:,0:10])
        
        y_pred = torch.mul(y_prime,0.001)
        return y_pred

##    ----------------------

## derived from TrackIntervalsToKDE_HDplusUNet100 & UNet
class TrackIntervalsToKDE_HDplusUNet100(nn.Module):
    softplus = torch.nn.Softplus()

    def __init__(self, nOut1=25, nOut2=25, nOut3=25,
                       nOut4=25, nOut5=25,
                       latentChannels=8,
                       n=64,
                       sc_mode='concat',
                       dropout_p=.25,
                       dropout_fc6=0.20,
                       d_selection='ConvBNrelu',
                       u_selection='Up'
                       ):
        super(TrackIntervalsToKDE_HDplusUNet100,self).__init__()

        self.nOut1 = nOut1
        self.nOut2 = nOut2
        self.nOut3 = nOut3
        self.nOut4 = nOut4
        self.nOut5 = nOut5

        self.latentChannels = latentChannels
       
####--------------- old HDplus code

        self.layer1 = nn.Linear(
                    in_features = 9,
                    out_features = self.nOut1,
                    bias = True)
        self.layer2 = nn.Linear(
                    in_features = self.layer1.out_features,
                    out_features = self.nOut2,
                    bias = True)
        self.layer3 = nn.Linear(
                    in_features = self.layer2.out_features,
                    out_features = self.nOut3,
                    bias = True)
        self.layer4 = nn.Linear(
                    in_features = self.layer3.out_features,
                    out_features = self.nOut4,
                    bias = True)
        self.layer5 = nn.Linear(
                    in_features = self.layer4.out_features,
                    out_features = self.nOut5,
                    bias = True)

## replace 4000 bin predicted features to be 100 for looking at 100-bin intervals
## mds 220714
        self.layer6A = nn.Linear(
                    in_features = self.layer5.out_features,
                    out_features = latentChannels*100,
                    bias = True)

        
# ======================================================================
#                            U-Net Models
# ======================================================================

## 220728  derived from Will's UNet model, but modified for use in 
##         tracks-to-hists architecture

## change the name to protect the innocent
##  mds '''
##  mds  This class exists in the event we want to do something with AWS or a grid search over different model architecture. This definition of a
##  mds  U-Net is more complicated than it needs to be, but it lets us specify the architecture using strings instead of objects.
##  mds  That way we would not need to write any code, if we were to do experiments on AWS, where we cannot directly interface with an IDE.
##  mds 
##  mds  Here is the link to the original U-Net paper: https://arxiv.org/abs/1505.04597
##  mds '''

        ## if we are doing "concatenation" skip connections, an output dimension of 16 during encoding will lead to an input dim of 32 during decoding.
        ## however, if we add instead of concatenate, we will have an input dimension of 16 during encoding AND decoding, so we do not need to scale up at all.
        ## We will multiply by this factor when we define our decoder, to make sure the shapes match up.
        if sc_mode == 'concat':
            factor = 2
        else:
            factor = 1
        self.mode = sc_mode
        self.p = dropout_p
        self.fc6dropout = nn.Dropout(dropout_fc6)

        ## make sure that if we configure the architecture using the strings, that the string is a valid choice
        assert d_selection in downsample_options.keys(), f'Selection for downsampling block {d_selection} not present in available options - {downsample_options.keys()}'
        assert u_selection in upsample_options.keys(), f'Selection for downsampling block {u_selection} not present in available options - {upsample_options.keys()}'

        ## make selection of which main component will be use in the decoder/encoder
        d_block = downsample_options[d_selection]
        u_block = upsample_options[u_selection]

        self.rcbn1 = d_block(self.latentChannels, n, kernel_size = 25, p=dropout_p)
        self.rcbn2 = d_block(n, n, kernel_size = 7, p=dropout_p)
        self.rcbn3 = d_block(n, n, kernel_size = 5, p=dropout_p)
## mds         self.rcbn4 = d_block(n, n, kernel_size = 5, p=dropout_p)
## mds         self.rcbn5 = d_block(n, n, kernel_size = 5, p=dropout_p)

        self.up1 = u_block(n, n, kernel_size = 5, p=dropout_p)
        self.up2 = u_block(n*factor, n, kernel_size = 5, p=dropout_p)
## mds         self.up3 = u_block(n*factor, n, kernel_size = 5, p=dropout_p)
## mds         self.up4 = u_block(n*factor, n, kernel_size = 5, p=dropout_p)
        self.out_intermediate = nn.Conv1d(n*factor, n, 5, padding=2)
        self.outc = nn.Conv1d(n, 1, 5, padding=2) # we need to project the n-dimensional output channels down to one, so we can call .squeeze() to remove it

        self.d = nn.MaxPool1d(2)

########  end of 220728 additions
    def forward(self, x):
        
## mds        print("in forward, x.shape = ",x.shape)
        leaky = nn.LeakyReLU(0.01)
        
        nEvts     = x.shape[0]
        nFeatures = x.shape[1]
        nTrks     = x.shape[2]
## mds        print("nEvts = ", nEvts,"   nFeatures = ", nFeatures, "  nTrks = ", nTrks)
        mask = x[:,0,:] > -98.
        filt = mask.float()
        f1 = filt.unsqueeze(2)
## mds 220714; again 4000 --> 100 when trying to predict intervals of 100 bins at a time
##        f2 = f1.expand(-1,-1,4000)
        f2 = f1.expand(-1,-1,100)
##        print("filt.shape = ",filt.shape)
##        print("f1.shape = ",f1.shape, "f2.shape = ",f2.shape)
        x = x.transpose(1,2)
##        print("after transpose, x.shape = ", x.shape)
        ones = torch.ones(nEvts,nFeatures,nTrks)
      
## make a copy of the initial features so they can be passed along using a skip connection 
        x0 = x 
        x = leaky(self.layer1(x))
        x = leaky(self.layer2(x))
        x = leaky(self.layer3(x))
        x = leaky(self.layer4(x))
        x = leaky(self.layer5(x))
        x = leaky(self.layer6A(x))  ## produces latentChannels x 100 bins for intervals
        x = self.fc6dropout(x)
##        print(' at point Aa, x.shape = ',x.shape)
       
        x = x.view(nEvts,nTrks,self.latentChannels,100)
##        print(' at point AA, x.shape = ',x.shape)

## here we are summing over all the tracks, creating "y"
## which has a sum of all tracks' contributions in each of
## latentChannels for each event and each bin of the (eventual)
## KDE histogram
##        print("before unsqueezing, f2.shape = ",f2.shape)
        f2 = torch.unsqueeze(f2,2)
##        print("x.shape = ",x.shape)
##        print("after unsqueezing,  f2 = torch.unsqueeze(f2,2), f2,shape = ",f2.shape)
        x = torch.mul(f2,x)
        y0 = torch.sum(x,dim=1)
##         print(' at point AB, y0.shape = ',y0.shape)

        ## the comments that follow after each line are the output sizes; the input size for HDplusUNet100 is 100
        x1 = self.rcbn1(y0) # 100
        x2 = self.d(self.rcbn2(x1)) # 50
        x = self.d(self.rcbn3(x2)) # 25

        x = self.up1(x) # 50
        x = self.up2(combine(x, x2, mode=self.mode)) # 100

        x = self.out_intermediate(combine(x, x1, mode=self.mode)) # 100
        logits_x0 = self.outc(x)

        y_prime = F.softplus(logits_x0).squeeze() # squeeze removes empty dimensions.. (64, 1, 100) -> (64, 100)


## 220728 mds ## begin to process the latentChannels contributions to
## 220728 mds ## the final KDE using two convolutional layers
## 220728 mds         y = leaky(self.conv1(y0))
## 220728 mds         y = self.conv1dropout(y)
## 220728 mds         y = leaky(self.conv2(y))
## 220728 mds         y = self.conv2dropout(y)
## 220728 mds ##        print('at point B, y.shape = ',y.shape)
## 220728 mds # Remove empty middle shape diminsion
## 220728 mds         y = y.view(y.shape[0], y.shape[-1])
## 220728 mds ####        print('at point Ba, y.shape = ',y.shape)
## 220728 mds         y = self.fc1(y)   ####  a fully connected layer
## 220728 mds ##        y = self.finalFilter(y)  #### a convolutional layer
## 220728 mds         y = y.view(nEvts,-1,100)
## 220728 mds ## ## ##        print('at point C, y.shape = ',y.shape)
## 220728 mds         y = self.softplus(y)
## 220728 mds 
## 220728 mds 
## 220728 mds         y_prime = y.view(-1,100)
##        print("y_prime.shape = ",y_prime.shape)

        y_pred = torch.mul(y_prime,0.001)
        return y_pred


##    ----------------------
## mds 220730 derived from TrackIntervalsToKDE_HDplusUNet100
##            to use 128-bin intervals rather than 100-bin intervals
class TrackIntervalsToKDE_HDplusUNet128(nn.Module):
    softplus = torch.nn.Softplus()

    def __init__(self, nOut1=25, nOut2=25, nOut3=25,
                       nOut4=25, nOut5=25,
                       latentChannels=8,
                       n=64,
                       sc_mode='concat',
                       dropout_p=.25,
                       d_selection='ConvBNrelu',
                       u_selection='Up'
                       ):
        super(TrackIntervalsToKDE_HDplusUNet128,self).__init__()

        self.nOut1 = nOut1
        self.nOut2 = nOut2
        self.nOut3 = nOut3
        self.nOut4 = nOut4
        self.nOut5 = nOut5

        self.latentChannels = latentChannels
       
####--------------- old HDplus code
## put this first so freezing the FC weights & biases will be easy

        self.layer1 = nn.Linear(
                    in_features = 9,
                    out_features = self.nOut1,
                    bias = True)
        self.layer2 = nn.Linear(
                    in_features = self.layer1.out_features,
                    out_features = self.nOut2,
                    bias = True)
        self.layer3 = nn.Linear(
                    in_features = self.layer2.out_features,
                    out_features = self.nOut3,
                    bias = True)
        self.layer4 = nn.Linear(
                    in_features = self.layer3.out_features,
                    out_features = self.nOut4,
                    bias = True)
        self.layer5 = nn.Linear(
                    in_features = self.layer4.out_features,
                    out_features = self.nOut5,
                    bias = True)

## replace 4000 bin predicted features to be 128 for looking at 128-bin intervals
## mds 220714
        self.layer6A = nn.Linear(
                    in_features = self.layer5.out_features,
                    out_features = latentChannels*128,
                    bias = True)

# ======================================================================
#                            U-Net Models
# ======================================================================

## 220728  derived from Will's UNet model, but modified for use in 
##         tracks-to-hists architecture

## change the name to protect the innocent
##  mds '''
##  mds  This class exists in the event we want to do something with AWS or a grid search over different model architecture. This definition of a
##  mds  U-Net is more complicated than it needs to be, but it lets us specify the architecture using strings instead of objects.
##  mds  That way we would not need to write any code, if we were to do experiments on AWS, where we cannot directly interface with an IDE.
##  mds 
##  mds  Here is the link to the original U-Net paper: https://arxiv.org/abs/1505.04597
##  mds '''

        ## if we are doing "concatenation" skip connections, an output dimension of 16 during encoding will lead to an input dim of 32 during decoding.
        ## however, if we add instead of concatenate, we will have an input dimension of 16 during encoding AND decoding, so we do not need to scale up at all.
        ## We will multiply by this factor when we define our decoder, to make sure the shapes match up.
        if sc_mode == 'concat':
            factor = 2
        else:
            factor = 1
        self.mode = sc_mode
        self.p = dropout_p

        ## make sure that if we configure the architecture using the strings, that the string is a valid choice
        assert d_selection in downsample_options.keys(), f'Selection for downsampling block {d_selection} not present in available options - {downsample_options.keys()}'
        assert u_selection in upsample_options.keys(), f'Selection for downsampling block {u_selection} not present in available options - {upsample_options.keys()}'

        ## make selection of which main component will be use in the decoder/encoder
        d_block = downsample_options[d_selection]
        u_block = upsample_options[u_selection]

        self.rcbn1 = d_block(self.latentChannels, n, kernel_size = 25, p=dropout_p)
        self.rcbn2 = d_block(n, n, kernel_size = 7, p=dropout_p)
        self.rcbn3 = d_block(n, n, kernel_size = 5, p=dropout_p)
        self.rcbn4 = d_block(n, n, kernel_size = 5, p=dropout_p)
        self.rcbn5 = d_block(n, n, kernel_size = 5, p=dropout_p)

        self.up1 = u_block(n, n, kernel_size = 5, p=dropout_p)
        self.up2 = u_block(n*factor, n, kernel_size = 5, p=dropout_p)
        self.up3 = u_block(n*factor, n, kernel_size = 5, p=dropout_p)
        self.up4 = u_block(n*factor, n, kernel_size = 5, p=dropout_p)
        self.out_intermediate = nn.Conv1d(n*factor, n, 5, padding=2)
        self.outc = nn.Conv1d(n, 1, 5, padding=2) # we need to project the n-dimensional output channels down to one, so we can call .squeeze() to remove it

        self.d = nn.MaxPool1d(2)

########  end of 220728 additions
        
    def forward(self, x):
        
## mds        print("in forward, x.shape = ",x.shape)
        leaky = nn.LeakyReLU(0.01)
        
        nEvts     = x.shape[0]
        nFeatures = x.shape[1]
        nTrks     = x.shape[2]
## mds        print("nEvts = ", nEvts,"   nFeatures = ", nFeatures, "  nTrks = ", nTrks)
        mask = x[:,0,:] > -98.
        filt = mask.float()
        f1 = filt.unsqueeze(2)
## mds 220714; again 4000 --> 100 when trying to predict intervals of 100 bins at a time
##        f2 = f1.expand(-1,-1,4000)
        f2 = f1.expand(-1,-1,128)
##        print("filt.shape = ",filt.shape)
##        print("f1.shape = ",f1.shape, "f2.shape = ",f2.shape)
        x = x.transpose(1,2)
##        print("after transpose, x.shape = ", x.shape)
        ones = torch.ones(nEvts,nFeatures,nTrks)
      
## make a copy of the initial features so they can be passed along using a skip connection 
        x0 = x 
        x = leaky(self.layer1(x))
        x = leaky(self.layer2(x))
        x = leaky(self.layer3(x))
        x = leaky(self.layer4(x))
        x = leaky(self.layer5(x))
        x = leaky(self.layer6A(x))  ## produces latentChannels x 128 bins for intervals
##        print(' at point Aa, x.shape = ',x.shape)
       
        x = x.view(nEvts,nTrks,self.latentChannels,128)
##        print(' at point AA, x.shape = ',x.shape)

## here we are summing over all the tracks, creating "y"
## which has a sum of all tracks' contributions in each of
## latentChannels for each event and each bin of the (eventual)
## KDE histogram
##        print("before unsqueezing, f2.shape = ",f2.shape)
        f2 = torch.unsqueeze(f2,2)
##        print("x.shape = ",x.shape)
##        print("after unsqueezing,  f2 = torch.unsqueeze(f2,2), f2,shape = ",f2.shape)
        x = torch.mul(f2,x)
        y0 = torch.sum(x,dim=1)
##         print(' at point AB, y0.shape = ',y0.shape)

        ## the comments that follow after each line are the output sizes; the input size for UNet128 is 128 bins
        x1 = self.rcbn1(y0) # 128
        x2 = self.d(self.rcbn2(x1)) # 64
        x3 = self.d(self.rcbn3(x2)) # 32
        x4 = self.d(self.rcbn4(x3)) # 16
        x = self.d(self.rcbn5(x4)) #   8

        x = self.up1(x) # 16
        x = self.up2(combine(x, x4, mode=self.mode)) # 32
        x = self.up3(combine(x, x3, mode=self.mode)) # 64
        x = self.up4(combine(x, x2, mode=self.mode)) # 128

        x = self.out_intermediate(combine(x, x1, mode=self.mode)) # 128
        logits_x0 = self.outc(x)

        y_prime = F.softplus(logits_x0).squeeze() # squeeze removes empty dimensions.. (64, 1, 128) -> (64, 128)


## 220728 mds ## begin to process the latentChannels contributions to
## 220728 mds ## the final KDE using two convolutional layers
## 220728 mds         y = leaky(self.conv1(y0))
## 220728 mds         y = self.conv1dropout(y)
## 220728 mds         y = leaky(self.conv2(y))
## 220728 mds         y = self.conv2dropout(y)
## 220728 mds ##        print('at point B, y.shape = ',y.shape)
## 220728 mds # Remove empty middle shape diminsion
## 220728 mds         y = y.view(y.shape[0], y.shape[-1])
## 220728 mds ####        print('at point Ba, y.shape = ',y.shape)
## 220728 mds         y = self.fc1(y)   ####  a fully connected layer
## 220728 mds ##        y = self.finalFilter(y)  #### a convolutional layer
## 220728 mds         y = y.view(nEvts,-1,100)
## 220728 mds ## ## ##        print('at point C, y.shape = ',y.shape)
## 220728 mds         y = self.softplus(y)
## 220728 mds 
## 220728 mds 
## 220728 mds         y_prime = y.view(-1,100)
##        print("y_prime.shape = ",y_prime.shape)

        y_pred = torch.mul(y_prime,0.001)
        return y_pred


##    ----------------------
